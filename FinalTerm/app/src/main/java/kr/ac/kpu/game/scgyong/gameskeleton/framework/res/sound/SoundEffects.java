package kr.ac.kpu.game.scgyong.gameskeleton.framework.res.sound;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.Log;

import java.util.HashMap;

import kr.ac.kpu.game.scgyong.gameskeleton.R;

public class SoundEffects {
    private static final String TAG = SoundEffects.class.getSimpleName();
    private static SoundEffects singleton;
    private SoundPool soundPool;
    private HashMap<Integer, Integer> soundIdMap = new HashMap<>();
    private static final int[] SOUND_IDS = {
       R.raw.die_sound, R.raw.eat_item,R.raw.eat_item_2,R.raw.shoot_sound
    };
    private int soundId;
    private Context context;
    private boolean loaded = false;
    private int streamId;

    public static SoundEffects get() {
        if (singleton == null) {
            singleton = new SoundEffects();
        }
        return singleton;
    }
    private SoundEffects() {
        AudioAttributes audioAttributes;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build();
            this.soundPool = new SoundPool.Builder()
                    .setAudioAttributes(audioAttributes)
                    .setMaxStreams(3)
                    .build();
        } else {
            this.soundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);
        }
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            public void onLoadComplete(SoundPool soundPool, int sampleId,int status) {
                loaded = true;
            }
        });
    }

    public boolean getloaded(){
        return loaded;
    }
    public void loadAll(Context context) {
        this.context = context;
        for (int resId: SOUND_IDS) {
            int soundId = soundPool.load(context, resId, 1);
            soundIdMap.put(resId, soundId);
        }
    }

    public int play(int resId) {
        if(soundIdMap.get(resId) ==null) {}
        else{ soundId = soundIdMap.get(resId);      }
        if(loaded) {
            Log.d(TAG,"444444444444444444444444444====" + soundId);
             streamId = soundPool.play(soundId,1f,1f,1,0,1f);
        }
        return streamId;
    }
}
