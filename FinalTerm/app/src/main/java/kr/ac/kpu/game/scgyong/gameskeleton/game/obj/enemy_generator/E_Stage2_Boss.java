package kr.ac.kpu.game.scgyong.gameskeleton.game.obj.enemy_generator;

import android.graphics.RectF;
import android.util.Log;

import java.util.ArrayList;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.iface.BoxCollidable;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.iface.LivingObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameTimer;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.UiBridge;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.AnimObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Bullet;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Effect;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Temp.Item;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator.H_Vaccine;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator.UI_Bar;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator.UI_HP;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_1;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_2;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_3;

public class E_Stage2_Boss extends AnimObject implements BoxCollidable, LivingObject {
    private static final String TAG = E_Stage2_Boss.class.getSimpleName();
    private static final int SPEED = 400;
    private static final float BULLET_SPEED = 5;
    public static final long DAFAULTTIME=100_000_0000;
    private final int maxlife;
    private final UI_Bar bar;
    private final UI_HP hp;
    protected float dx, dy;
    private int damage;
    private int life;
    private long lastFire;
    private float x_angle, y_angle;
    private int wall_x=0;
    private boolean wall_x_on=true;

    public E_Stage2_Boss(float x,float y,float dx,float dy) {
        super(x, y, 0, 0, R.mipmap.boss_2, 12, 5);
        this.dx = dx;
        this.dy = dy;
        life = 40;
        maxlife=40;
        this.damage = 1;
        bar = new UI_Bar(0, 10);
        hp = new UI_HP(0, 10,getLife(),maxlife);
        bar.setWidth(1200);
        bar.setHeight(30);
        hp.setWidth(1200);
        hp.setHeight(30);
        Stage_2.get().getGameWorld().add(Stage_2.Layer.ui.ordinal(), hp);
        Stage_2.get().getGameWorld().add(Stage_2.Layer.ui.ordinal(), bar);
    }
    public void removeUI(){
        hp.remove();
        bar.remove();}
    @Override
    public void update() {
        //체력바
        bar.setPos(10,10);
        hp.setPos(10,10,life);
        ArrayList<GameObject> heros = Stage_2.get().getGameWorld().objectsAtLayer(Stage_2.Layer.player.ordinal());

        for (GameObject obj : heros) {
            if (!(obj instanceof H_Vaccine)) {
                continue;
            }
            H_Vaccine hero = (H_Vaccine) obj;
        }
        float heroposX = heros.get(0).getX();
        float heroposY = heros.get(0).getY();
        float x_dest =  heroposX - x;
        float y_dest =  heroposY - y;
        double rad = Math.atan2(y_dest, x_dest);
        //총알 패턴
        long now = GameTimer.getCurrentTimeNanos();
        long elapsed = now - lastFire;

        if (elapsed > DAFAULTTIME *0.01) {

            //좌우 벽
            Wall_fire((int)x+360+wall_x,(int)y);
            Wall_fire((int)x-350+wall_x,(int)y);
            if(wall_x_on){wall_x+=1;}
            else{wall_x-=1;}
            if(wall_x ==200){wall_x_on=false;}
            if(wall_x==0){wall_x_on=true;}
            //플레이어를 따라 발사
            if (elapsed > DAFAULTTIME *2) {
                x_angle = BULLET_SPEED * (float)Math.cos(rad);
                y_angle = BULLET_SPEED * (float)Math.sin(rad);
                fire();
                lastFire = now;
            }
        }
    }

    //    @Override
//    public boolean onTouchEvent(MotionEvent e) {
//
//    }
    @Override
    public void getBox(RectF rect) {
        int hw = width / 2;
        int hh = height / 2;
        rect.left = x - hw;
        rect.top = y - hh;
        rect.right = x + hw;
        rect.bottom = y + hh;
    }

    private void Wall_fire(int xpos, int ypos) {
        Bullet bullet = new Bullet(xpos, ypos,50,50, R.mipmap.temp_bullet_7, 0, 500,damage,false, 2);
        //dx x축 속도 , dy y축 속도
        Stage_2.get().getGameWorld().add(Stage_1.Layer.e_bullet.ordinal(), bullet);
    }
    private void fire() {
        Bullet bullet = new Bullet(x, y,100,100, R.mipmap.temp_bullet_3, 200*x_angle, 200*y_angle,damage,false, 2,4);
        //dx x축 속도 , dy y축 속도
        Stage_2.get().getGameWorld().add(Stage_1.Layer.e_bullet.ordinal(), bullet);
    }

    @Override
    public int getLife() {

        return life;
    }

    @Override
    public void setLife(float life) {
        this.life += life;
    }

    @Override
    public int getDamage() {
        return damage;
    }

    @Override
    public void setDamage(int damage) {
        this.life -= damage;
        SoundEffects.get().play(R.raw.die_sound);
        if(this.life == 0) {
            remove();
            removeUI();
            Effect effect = new Effect(x, y, 200, 200, R.mipmap.dead6, 40, 16, 1);
            Stage_2.get().getGameWorld().add(Stage_2.Layer.ui.ordinal(), effect);
            Stage_2.get().setScoreObject(4000);
            Stage_2.get().SetBossStatus(true);
        }
    }
}
