package kr.ac.kpu.game.scgyong.gameskeleton.game.scene;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameScene;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.UiBridge;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.BitmapObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.ui.Button;
import kr.ac.kpu.game.scgyong.gameskeleton.ui.activity.GameActivity;

public class OptionScene extends GameScene {
    private static final String TAG = OptionScene.class.getSimpleName();
    private GameActivity activity;

    public enum Layer {
        bg, enemy, player, ui, COUNT
    }

    void setActivity(GameActivity activity)
    {
        this.activity = activity;
    }
    @Override
    protected int getLayerCount() {
        return Layer.COUNT.ordinal();
    }

    @Override
    public void enter() {
        super.enter();
        setTransparent(true);
        initObjects();
    }


    private void initObjects() {
        int screenWidth = UiBridge.metrics.size.x;
        int screenHeight = UiBridge.metrics.size.y;

        int cx = UiBridge.metrics.center.x;
        int y = UiBridge.metrics.center.y-300;

        gameWorld.add(Layer.bg.ordinal(), new BitmapObject(cx, y+300, screenWidth, screenHeight, R.mipmap.black_transparent));
        gameWorld.add(Layer.bg.ordinal(), new BitmapObject(cx, y+200, 1000, 1200, R.mipmap.option_bg));
//        y += UiBridge.y(100);
        Button btn_goback = new Button(cx, y, R.mipmap.btn_resume, R.mipmap.blue_round_btn, R.mipmap.red_round_btn);
        btn_goback.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                pop();

            }
        });
        gameWorld.add(Layer.ui.ordinal(), btn_goback);
////////////////////////
        y += UiBridge.y(60);
        Button btn_restart = new Button(cx, y, R.mipmap.btn_restart, R.mipmap.blue_round_btn, R.mipmap.red_round_btn);
        btn_restart.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                pop();
                Stage_1 scene = new Stage_1();
                scene.setActivity(activity);
                scene.replace();

            }
        });
        gameWorld.add(Layer.ui.ordinal(), btn_restart);
        y += UiBridge.y(60);
////////////////////////
        Button btn_exit = new Button(cx, y, R.mipmap.btn_exit, R.mipmap.blue_round_btn, R.mipmap.red_round_btn);
        btn_exit.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
              pop();
              Start_Menu scene = new Start_Menu();
              scene.setActivity(activity);
              scene.replace();
            }
        });
        gameWorld.add(Layer.ui.ordinal(), btn_exit);
////////////////////////
    }
}
