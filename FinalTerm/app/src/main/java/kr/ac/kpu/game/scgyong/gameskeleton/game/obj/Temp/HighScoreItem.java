package kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Temp;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class HighScoreItem {
    public String name;
    public Date date;
    public int score;
    public int rank;

    public HighScoreItem(String name, Date date, int score){
        this.name =name;
        this.date =date;
        this.score =score;
    }

    public int getScore(){
        return score;
    }
    public  int getRank(){
        return rank;
    }
    public void setRank(int rank){
        this.rank = rank;
    }

    public HighScoreItem(JSONObject s) throws JSONException {
        this.name =s.getString("name");
        long dateValue = s.getLong("date");
        this.date = new Date(dateValue);
        this.score =s.getInt("score");
        this.rank = s.getInt("rank");
    }
    public String toJsonString() {
        return "{\"name\":\"" + name + "\" , \"date\":" + date.getTime() + ",\"score\":" +score +",\"rank\":" +rank + "}";
    }

    public JSONObject toJsonObject() {
        JSONObject s = new JSONObject();
        try{
            s.put("name",name);
            s.put("date",date);
            s.put("score",score);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return s;

    }
}
