package kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameTimer;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameWorld;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.BitmapObject;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Bullet;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_1;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_2;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_3;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Tutorial;

public class H_SubVaccine extends BitmapObject{

    private static final long BULLET_FIRE_INTERVAL_NSEC = 100_000_0000;
    private final int stageNum;
    private long lastFire;
    private int damage;
    private boolean StopFire;
    private GameWorld gw;

    public H_SubVaccine(float x, float y, int damage, int stageNum) {
        super(x, y, 40,75, R.mipmap.vaccine);
        this.damage = damage;
        this.stageNum = stageNum;
    }
    public void StopFIreSwitch(boolean stopFire){ StopFire =stopFire;   }

    @Override
    public float getRadius() {
        return this.width / 4;
    }
    public void update() {
        long now = GameTimer.getCurrentTimeNanos();
        long elapsed = now - lastFire;
        if(stageNum==4) {
            if (elapsed > BULLET_FIRE_INTERVAL_NSEC) {
                    fire();
                    lastFire = now;
            }
        }
        else {
            if (elapsed > BULLET_FIRE_INTERVAL_NSEC) {
                if (StopFire) {
                    fire();
                    lastFire = now;
                }
            }
        }
    }

    private void fire() {
        Bullet bullet = new Bullet(x , y-45 ,30,50, R.mipmap.bullet2,0, -500, damage,true,stageNum);
        if(stageNum==1) {
            Stage_1.get().getGameWorld().add(Stage_1.Layer.h_bullet.ordinal(), bullet);
        }else if(stageNum ==2) {
            Stage_2.get().getGameWorld().add(Stage_2.Layer.h_bullet.ordinal(), bullet);
        }else if(stageNum ==3) {
            Stage_3.get().getGameWorld().add(Stage_3.Layer.h_bullet.ordinal(), bullet);
        }else if(stageNum ==4) {
            Tutorial.get().getGameWorld().add(Tutorial.Layer.h_bullet.ordinal(), bullet);
        }
    }


    public void setPos(float x, float y) {
        this.x = x;
        this.y = y;
    }
}
