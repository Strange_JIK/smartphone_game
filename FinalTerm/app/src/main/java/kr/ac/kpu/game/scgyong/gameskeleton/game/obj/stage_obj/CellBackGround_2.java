package kr.ac.kpu.game.scgyong.gameskeleton.game.obj.stage_obj;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.bg.ImageScrollBackground;

public class CellBackGround_2 extends ImageScrollBackground {
    public CellBackGround_2() {
        super(R.mipmap.cell_map_2, Orientation.vertical, 300);
    }
}
