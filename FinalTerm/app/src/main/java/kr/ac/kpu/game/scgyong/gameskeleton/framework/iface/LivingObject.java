package kr.ac.kpu.game.scgyong.gameskeleton.framework.iface;

public interface LivingObject {
    int getLife();
    void setLife(float life);
    int getDamage();
    void setDamage(int damage);

}
