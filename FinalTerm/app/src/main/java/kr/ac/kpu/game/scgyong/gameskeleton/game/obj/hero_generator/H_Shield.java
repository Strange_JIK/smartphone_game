package kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.BitmapObject;

public class H_Shield extends BitmapObject {

    public H_Shield(float x, float y)
    {
        super(x, y, 150,150, R.mipmap.sheild);
    }

    @Override
    public float getRadius() {
        return this.width / 4;
    }
    public void update() { }


    public void setPos(float x, float y) {
        this.x = x;
        this.y = y;
    }
}
