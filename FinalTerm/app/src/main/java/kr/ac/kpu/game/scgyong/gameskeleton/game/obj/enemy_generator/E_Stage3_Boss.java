package kr.ac.kpu.game.scgyong.gameskeleton.game.obj.enemy_generator;

import android.graphics.RectF;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.Random;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.iface.BoxCollidable;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.iface.LivingObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameTimer;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.UiBridge;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.AnimObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Bullet;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Effect;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator.H_Vaccine;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator.UI_Bar;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator.UI_HP;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.GameClearScene;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_1;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_2;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_3;
import kr.ac.kpu.game.scgyong.gameskeleton.ui.activity.GameActivity;

public class E_Stage3_Boss extends AnimObject implements BoxCollidable, LivingObject {
    private static final String TAG = E_Stage3_Boss.class.getSimpleName();
    private static final int SPEED = 400;
    private static final float BULLET_SPEED = 5;
    public static final long DAFAULTTIME=100_000_0000;
    private final int maxlife;
    private final UI_Bar bar;
    private final UI_HP hp;
    private final GameClearScene GameClearscene;
    protected float dx, dy;
    private int damage;
    private int life;
    private long lastFire;
    private int xdir=1;
    private float x_angle, y_angle;
    private int count =0;
    private float B_xPos =0,B_yPos =0;
    private int radian;
    private int angle;
    private E_Stage3_Boss_Soldier minimi;
    private BaseAdapter adapter;
    private GameActivity activity;

    public E_Stage3_Boss(float x,float y,float dx,float dy) {
        super(x, y, 0, 0, R.mipmap.boss_3, 12, 5);
        this.dx = dx;
        this.dy = dy;
        life = 50;
        maxlife=50;
        this.damage = 1;
        bar = new UI_Bar(0, 10);
        hp = new UI_HP(0, 10,getLife(),maxlife);
        bar.setWidth(1200);
        bar.setHeight(30);
        hp.setWidth(1200);
        hp.setHeight(30);
        Stage_3.get().getGameWorld().add(Stage_3.Layer.ui.ordinal(), hp);
        Stage_3.get().getGameWorld().add(Stage_3.Layer.ui.ordinal(), bar);
        GameClearscene = new GameClearScene();
    }
    public void removeUI(){
        hp.remove();
        bar.remove();}
    @Override
    public void update() {
        //체력바
        bar.setPos(10,10);
        hp.setPos(10,10,life);
        /////
        ArrayList<GameObject> heros = Stage_3.get().getGameWorld().objectsAtLayer(Stage_3.Layer.player.ordinal());
        ArrayList<GameObject> mini_boss = Stage_3.get().getGameWorld().objectsAtLayer(Stage_3.Layer.enemy.ordinal());
        ArrayList<GameObject> boss_bullet = Stage_3.get().getGameWorld().objectsAtLayer(Stage_3.Layer.e_bullet.ordinal());

        float heroposX = heros.get(0).getX();
        float heroposY = heros.get(0).getY();
        float x_dest = heroposX - x;
        float y_dest = heroposY - y;
        double ang = Math.atan2(y_dest,x_dest);
        //총알 패턴
        long now = GameTimer.getCurrentTimeNanos();
        long elapsed = now - lastFire;

        //플레이어를 따라 발사
        if(life>35) {
            if(radian>=300){
                angle+=1;
                radian =0;
            }
            else
            {
                B_xPos= radian * (float)Math.cos(angle * Math.PI/180);
                B_yPos= radian *(float)Math.sin(angle* Math.PI/180);
                x_angle= radian * (float)Math.cos(angle * Math.PI/180);
                y_angle= radian *(float)Math.sin(angle* Math.PI/180);
                fire();
                lastFire = now;
                angle+=15;
                radian+=5;

            }
        }
        else if(life ==15) {
            /////
            for(int i=0;i<boss_bullet.size();i++) {
                boss_bullet.get(i).remove();
            }
        }

        else {

            /////
            for (GameObject obj : mini_boss) {
                if (!(obj instanceof E_Stage3_Boss_Soldier)) {
                    continue;
                }
                minimi = (E_Stage3_Boss_Soldier) obj;
                minimi.check_phase(true);
            }
            //////
            if (elapsed > DAFAULTTIME * 1) {
                B_xPos=0;
                B_yPos=0;
                Random r = new Random();
                float min = -UiBridge.metrics.size.x, max = UiBridge.metrics.size.x;
                for (int i = 0; i < 30; i++) {
                    float random_x = min + r.nextFloat() * (max - min);
                    float random_y = min + r.nextFloat() * (max - min);
                    x_angle = random_x;
                    y_angle = random_y;
                    fire();
                    lastFire = now;
                }
            }

        }

    }

    //    @Override
//    public boolean onTouchEvent(MotionEvent e) {
//
//    }
    @Override
    public void getBox(RectF rect) {
        int hw = width / 2;
        int hh = height / 2;
        rect.left = x - hw;
        rect.top = y - hh;
        rect.right = x + hw;
        rect.bottom = y + hh;
    }


    private void fire() {
        Bullet bullet = new Bullet(x+B_xPos, y+B_yPos,50,50, R.mipmap.temp_bullet_4, 1*x_angle, 1*y_angle,damage,false, 3);
        //dx x축 속도 , dy y축 속도
        Stage_3.get().getGameWorld().add(Stage_3.Layer.e_bullet.ordinal(), bullet);
    }

    @Override
    public int getLife() {

        return life;
    }

    @Override
    public void setLife(float life) {
        this.life += life;
    }

    @Override
    public int getDamage() {
        return damage;
    }

    @Override
    public void setDamage(int damage) {
        this.life -= damage;
        SoundEffects.get().play(R.raw.die_sound);
        if(this.life == 0) {
            remove();
            removeUI();
            minimi.remove();
            Effect effect = new Effect(x, y, 200, 200, R.mipmap.dead6, 40, 16, 1);
            Stage_3.get().getGameWorld().add(Stage_3.Layer.ui.ordinal(), effect);
            Stage_3.get().setScoreObject(7000);
            Stage_3.get().SetBossStatus(true);
            GameClearscene.setActivity(activity);
            GameClearscene.setAdapter(adapter);
            GameClearscene.setScore(Stage_3.get().getScoreValue());
            GameClearscene.push();
        }
    }

    public void setAdapter(BaseAdapter adapter) {
        this.adapter = adapter;
    }

    public void setActivity(GameActivity activity) {
        this.activity = activity;
    }
}
