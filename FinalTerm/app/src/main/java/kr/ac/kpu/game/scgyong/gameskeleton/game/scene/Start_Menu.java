package kr.ac.kpu.game.scgyong.gameskeleton.game.scene;

import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Random;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameScene;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameTimer;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.UiBridge;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.BitmapObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.ScoreObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.ui.Button;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Temp.HighScoreItem;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Temp.Serializer;
import kr.ac.kpu.game.scgyong.gameskeleton.ui.activity.GameActivity;

public class Start_Menu extends GameScene {
    private static final String TAG = Start_Menu.class.getSimpleName();
    private int xx;
    private MotionEvent e;
    private Object mPauseLock;
    private boolean mPaused;
    private GameActivity activity;

    private ListView listView;
    private ArrayList<HighScoreItem> scores = new ArrayList<>();
    private View view;
    private int scoreValue;
    private MediaPlayer mp;
    private boolean musicon=false;

    public void setScoreValue(int scoreValue) {
        this.scoreValue = scoreValue;
    }


    public enum Layer {
        bg, enemy, player, ui, COUNT
    }

    private ScoreObject scoreObject;
    private GameTimer timer;

    @Override
    protected int getLayerCount() {
        return Layer.COUNT.ordinal();
    }

    @Override
    public void update() {
        super.update();
//        System.exit(0);
        long now = GameTimer.getCurrentTimeNanos();
        ///////bgm
        if(activity!=null)
        {
            if(!musicon){
            mp = MediaPlayer.create(activity,R.raw.vvodka_8bit);
            Log.d(TAG,"onononono");
            mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer player) {
                    player.start();
                    musicon=true;
                }
            });
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer player) {
                    if (mp != null) {
                        mp.release();
                        mp = null;
                    }
                }
            });
            }
        }
        ////////////////

//        Log.d(TAG, "time: " + now);
    }
    public void setView(View view)
    {
        this.view = view;
    }

    public void setActivity(GameActivity activity)
    {
        this.activity = activity;
    }

    @Override
    public void enter() {
        super.enter();
        initObjects();
        Log.d(TAG,""+getLayerCount());

    }

    @Override
    public void exit() {
        super.exit();
        mp.release();
        musicon=false;
    }

    @Override
    public void pause() {

    }

    private void initObjects() {
        BitmapObject title = new BitmapObject(UiBridge.metrics.center.x, UiBridge.metrics.size.y/2, -69, -86, R.mipmap.mainmenu);
        setActivity(activity);
        gameWorld.add(Layer.ui.ordinal(), title);
        int cx = UiBridge.metrics.center.x;
        int y = UiBridge.metrics.center.y;
//        y += UiBridge.y(100);

        Button button_start = new Button(cx, y, R.mipmap.btn_start_game, R.mipmap.blue_round_btn, R.mipmap.red_round_btn);
        button_start.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                mp.release();
                Stage_1 scene = new Stage_1();
                scene.setActivity(activity);
                scene.setAdapter(adapter);
                scene.push();
            }
        });
        gameWorld.add(Layer.ui.ordinal(), button_start);
////////////////////////
        y += UiBridge.y(100);
////////////////////////

        Button button_score = new Button(cx, y, R.mipmap.btn_highscore, R.mipmap.blue_round_btn, R.mipmap.red_round_btn);
        button_score.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                mp.release();
                activity.setContentView(R.layout.activity_rank);
                scores = Serializer.load(activity);
                listView = activity.findViewById(R.id.listview);
                listView.setAdapter(adapter);
            }
        });
        gameWorld.add(Layer.ui.ordinal(), button_score);
////////////////////////
        y += UiBridge.y(100);
////////////////////////
        Button button_tuto = new Button(cx, y, R.mipmap.btn_tutorial, R.mipmap.blue_round_btn, R.mipmap.red_round_btn);

        button_tuto.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                mp.release();
                Tutorial scene = new Tutorial();
                scene.push();
            }
        });
        gameWorld.add(Layer.ui.ordinal(), button_tuto);
////////////////////////
    }

    BaseAdapter adapter = new BaseAdapter() {
        @Override
        public int getCount() {
            return scores.size();
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if(view ==null){
                LayoutInflater inflater = activity.getLayoutInflater();
                view = inflater.inflate(R.layout.score_item,null);
            }
            TextView Id = view.findViewById(R.id.IdTextView);
            TextView Score = view.findViewById(R.id.ScoreTextView);
            ImageView im = view.findViewById(R.id.scoreItemImageView);

//            tv.setText("The Itme"+ position);
            HighScoreItem S = scores.get(position);
            if(S.rank ==1){im.setImageResource(R.mipmap.gold);}
            else if(S.rank ==2){im.setImageResource(R.mipmap.silver);}
            else if(S.rank ==3){im.setImageResource(R.mipmap.cooper);}
            else {im.setImageResource(R.mipmap.medal);}
            String d = S.date.toString();
            SimpleDateFormat formatType = new SimpleDateFormat("MM-dd-h:mm:ss a");
            Id.setText(" "+S.name +" " );
            Score.setText(" " + S.score+" " );
            return view;
        }
        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }
    };

    public void onBtnAdd(View view) {
        final EditText et = new EditText(activity);
        new AlertDialog.Builder(activity)
                .setTitle(R.string.highscore)
                .setMessage("아이디를 입력해주세요")
                .setView(et)
                .setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String name = et.getText().toString();
                        addHighscore(name,scoreValue);
                    }
                })
                .setNegativeButton(R.string.cancel,null)
                .create()
                .show();
    }

    public void onBtnDelete(View view) {
        scores.clear();
        adapter.notifyDataSetChanged();
    }

    public void sortHighscore() {
        Comparator<HighScoreItem> cmp = new Comparator<HighScoreItem>() {
            @Override
            public int compare(HighScoreItem o1, HighScoreItem o2) {
                int ret ;

                if (o1.getScore() > o2.getScore())
                    ret = -1 ;
                else if (o1.getScore() == o2.getScore())
                    ret = 0 ;
                else
                    ret = 1 ;
                return ret ;
            }
        } ;
        Collections.sort(scores, cmp);
        adapter.notifyDataSetChanged();
    }

    public void addHighscore(String name, int score){
        scores.add(new HighScoreItem(name, new Date(),score));
        sortHighscore();
        giveRank();
        Serializer.save(activity,scores);
        adapter.notifyDataSetChanged();
    }

    public void giveRank() {
        for(int i=0; i<scores.size(); i++)
        {
            scores.get(i).setRank(i+1);
        }
        adapter.notifyDataSetChanged();
    }

}
