package kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.BitmapObject;

public class UI_HP extends BitmapObject  {

    private static final int bitmapid = 1;
    private int life;
    private  int maxlife;
    private  int nowwidth;

    public UI_HP(float x, float y,int life, int maxlife) {
        super(x, y, 120,20 , R.mipmap.hp, bitmapid);
        this.life = life;
        this.maxlife = maxlife;
        nowwidth =120;
    }

    public void update() {
        this.width = life*(nowwidth/maxlife);
    }
    public void setPos(float x,float y,int life) {
        this.x = x;
        this.y = y;
        this.life = life;
    }
    public void setWidth(int width){
        this.width = width;
        nowwidth=width;
    }
    public void setHeight(int height){
        this.height = height;
    }
}
