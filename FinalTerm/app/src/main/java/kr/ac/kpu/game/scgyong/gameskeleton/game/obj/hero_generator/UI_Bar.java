package kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.BitmapObject;

public class UI_Bar extends BitmapObject  {

    private int nowWidth;
    public UI_Bar(float x, float y) {
        super(x, y, 120,20 , R.mipmap.bar, 1);
        nowWidth= 120;
        this.width = nowWidth;
    }

    @Override
    public float getRadius() {
        return this.width / 4;
    }
    public void update() {}

    public void setPos(float x,float y) {
        this.x = x;
        this.y = y;
    }
    public void setWidth(int width){
        this.width = width;
        nowWidth=width;
    }
    public void setHeight(int height){
        this.height = height;
    }
}
