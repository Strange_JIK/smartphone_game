package kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator;

import android.graphics.RectF;
import android.view.MotionEvent;
import android.view.View;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.iface.BoxCollidable;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.iface.LivingObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.iface.Touchable;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameTimer;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.UiBridge;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.AnimObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.res.bitmap.FrameAnimationBitmap;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.util.CollisionHelper;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Bullet;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Temp.Item;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.enemy_generator.E_Guided_Virus;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.enemy_generator.E_Mitosis_Virus;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.enemy_generator.E_Virus;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.GameOverScene;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_1;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_2;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_3;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Tutorial;
import kr.ac.kpu.game.scgyong.gameskeleton.ui.activity.GameActivity;

public class H_Vaccine extends AnimObject implements Touchable, BoxCollidable, LivingObject {

    private static final String TAG = H_Vaccine.class.getSimpleName();
    public static final int BULLET_FIRE_INTERVAL_NSEC = 300_000_000;
    private static final int SPEED = 500;
    public static final long DAFAULTTIME=100_000_0000;
    private final FrameAnimationBitmap fabNormal;
    private final FrameAnimationBitmap fabinvisible;
    private final FrameAnimationBitmap fabRight;
    private final int damage;
    private GameOverScene GameOverscene = null;
    private  View view;
    private H_Heart heroHeart3;
    private H_Heart heroHeart2;
    private H_Heart heroHeart1;
    private final int stageNum;
    private int life;

    private float speed;
    private float base;
    private long lastFire;
    private long lastDamage;
    private boolean down;
    private float angle;
    private float xPos;
    private float yPos;
    private H_Shield HShield;
    private int shieldCnt;
    private long antiattack;
    private int vaccineCnt;
    private H_SubVaccine leftVaccine;
    private H_SubVaccine rightVaccine;
    private long Stage_time;
    private float xDown;
    private float yDown;
    public boolean StopFire;
    private GameActivity activity;
    private BaseAdapter adapter;
    public  boolean NoDamage =false;
    private int invisible_count=0;

    public H_Vaccine(float x, float y, int stageNum) {
        super(x, y, 50, 150, R.mipmap.injection_niddle, 1, 1);
        base = y;
        fabNormal = fab;
        fabinvisible = new FrameAnimationBitmap(R.mipmap.injection_niddle_invisible, 12, 1);
        fabRight = new FrameAnimationBitmap(R.mipmap.enemy_yellow, 12, 1);
        life = 3;
        damage = 1;
        this.stageNum = stageNum;

        if(stageNum==1) {
            heroHeart1 = new H_Heart(UiBridge.metrics.size.x / 2 - 100, UiBridge.metrics.size.y - 50);
            Stage_1.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), heroHeart1);

            heroHeart2 = new H_Heart(UiBridge.metrics.size.x / 2, UiBridge.metrics.size.y - 50);
            Stage_1.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), heroHeart2);

            heroHeart3 = new H_Heart(UiBridge.metrics.size.x / 2 + 100, UiBridge.metrics.size.y - 50);
            Stage_1.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), heroHeart3);
        }
        else if(stageNum==2) {
            heroHeart1 = new H_Heart(UiBridge.metrics.size.x / 2 - 100, UiBridge.metrics.size.y - 50);
            Stage_2.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), heroHeart1);

            heroHeart2 = new H_Heart(UiBridge.metrics.size.x / 2, UiBridge.metrics.size.y - 50);
            Stage_2.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), heroHeart2);

            heroHeart3 = new H_Heart(UiBridge.metrics.size.x / 2 + 100, UiBridge.metrics.size.y - 50);
            Stage_2.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), heroHeart3);
        }
        else if(stageNum==3) {
            heroHeart1 = new H_Heart(UiBridge.metrics.size.x / 2 - 100, UiBridge.metrics.size.y - 50);
            Stage_3.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), heroHeart1);

            heroHeart2 = new H_Heart(UiBridge.metrics.size.x / 2, UiBridge.metrics.size.y - 50);
            Stage_3.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), heroHeart2);

            heroHeart3 = new H_Heart(UiBridge.metrics.size.x / 2 + 100, UiBridge.metrics.size.y - 50);
            Stage_3.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), heroHeart3);
        }
        else if(stageNum == 4) {
            heroHeart1 = new H_Heart(UiBridge.metrics.size.x / 2 - 100, UiBridge.metrics.size.y - 50);
            Tutorial.get().getGameWorld().add(Tutorial.Layer.ui.ordinal(), heroHeart1);

            heroHeart2 = new H_Heart(UiBridge.metrics.size.x / 2, UiBridge.metrics.size.y - 50);
            Tutorial.get().getGameWorld().add(Tutorial.Layer.ui.ordinal(), heroHeart2);

            heroHeart3 = new H_Heart(UiBridge.metrics.size.x / 2 + 100, UiBridge.metrics.size.y - 50);
            Tutorial.get().getGameWorld().add(Tutorial.Layer.ui.ordinal(), heroHeart3);
        }

        GameOverscene = new GameOverScene();

    }

    @Override
    public int getLife() {
        return life;
    }

    @Override
    public void setLife(float life) {
        this.life++;
    }

    @Override
    public int getDamage() {
        return damage;
    }

    @Override
    public void setDamage(int damage) {
        if(!NoDamage) {
            this.life -= damage;

            NoDamage=true;
            if (this.life == 2) {
                heroHeart3.remove();
            } else if (life == 1) {
                heroHeart2.remove();
            } else if (life == 0) {
                if(stageNum==4)
                    return;
                this.remove();
                heroHeart1.remove();
                if (leftVaccine != null && rightVaccine != null) {
                    leftVaccine.remove();
                    rightVaccine.remove();
                    vaccineCnt = 0;
                }
                if (stageNum == 1) {
                    if(activity==null) {
                        System.out.println("1.널이야");
                        System.out.println(activity);
                        return;
                    }
                    System.out.println("1.널아니야");
                    System.out.println(activity);
                    GameOverscene.setActivity(activity);
                    GameOverscene.setAdapter(adapter);
                    GameOverscene.setScore(Stage_1.get().getScoreValue());
                    GameOverscene.push();
                } else if (stageNum == 2) {
                    if(activity==null) {
                        System.out.println("2.널이야");
                        System.out.println(activity);
                        return;
                    }
                    System.out.println("2.널아니야");
                    System.out.println(activity);
                    GameOverscene.setActivity(activity);
                    GameOverscene.setAdapter(adapter);
                    GameOverscene.setScore(Stage_2.get().getScoreValue());
                    GameOverscene.push();
                } else if (stageNum == 3) {
                    GameOverscene.setActivity(activity);
                    GameOverscene.setAdapter(adapter);
                    GameOverscene.setScore(Stage_3.get().getScoreValue());
                    GameOverscene.push();
                }
            }
        }
    }

    public void SetGameTime(long stage_time) {
        Stage_time = stage_time;
    }

    public void setActivity(GameActivity activity) {
        this.activity = activity;
    }

    public void setAdapter(BaseAdapter adapter) {
        this.adapter = adapter;
    }


    public enum AnimState {
        normal,invisible, right
    }

    public void setAnimState(AnimState state) {
        if (state == AnimState.normal) {
            fab = fabNormal;
        } else if (state == AnimState.invisible){
            fab = fabinvisible;
        } else if (state == AnimState.right){
            fab = fabRight;
        }
    }
    public void StopFIreSwitch(boolean stopFire){
        StopFire = stopFire;
        if(rightVaccine != null && leftVaccine != null&& vaccineCnt>0) {
            leftVaccine.StopFIreSwitch(stopFire);
            rightVaccine.StopFIreSwitch(stopFire);
        }
    }
    @Override
    public void update() {

        long now = GameTimer.getCurrentTimeNanos();
        long elapsed = now - lastFire;
        long invisible_time = now - lastDamage;
        if(elapsed > BULLET_FIRE_INTERVAL_NSEC) {
            if(StopFire) {
                SoundEffects.get().play(R.raw.shoot_sound);
                fire();
                lastFire = now;
            }
        }

        int xdir = getHorzDirection();
        x += xdir*SPEED* GameTimer.getTimeDiffSeconds();

        if(x < 0) {
            x=0;
        }
        else if(x>UiBridge.metrics.size.x) {
            x = UiBridge.metrics.size.x;
        }
        ///////////무적
        if(invisible_time > DAFAULTTIME*0.1) {
            if(NoDamage) {
                if(invisible_count>3)
                {
                    ////////////////3초간 무적

                    invisible_count=0;
                    NoDamage=false;
                }
                setAnimState(AnimState.invisible);
                invisible_count += 1;
                lastDamage = now;
            }
            else{
                setAnimState(AnimState.normal);
            }
        }
        ///////////////////////
        //Log.d(TAG,""+NoDamage);
        //Log.d(TAG,""+invisible_count);
        checkItemCollision();
        if(!NoDamage)
        {
            checkBulletCollision();
            checkVirusCollision();
        }
        if(HShield != null&& shieldCnt==1) {
            HShield.setPos(x,y);
        }
        if(rightVaccine != null && leftVaccine != null) {
            leftVaccine.setPos(x - 50,y + 40);
            rightVaccine.setPos(x + 50,y + 40);
        }
    }

    private void fire() {
        Bullet bullet = new Bullet(x , y-140 ,60,60, R.mipmap.bullet7,0, -500, damage,true, stageNum);
        if(stageNum == 1) {
            Stage_1.get().getGameWorld().add(Stage_1.Layer.h_bullet.ordinal(), bullet);
        } else if(stageNum == 2) {
            Stage_2.get().getGameWorld().add(Stage_2.Layer.h_bullet.ordinal(), bullet);
        } else if(stageNum == 3) {
            Stage_3.get().getGameWorld().add(Stage_3.Layer.h_bullet.ordinal(), bullet);
        } else if(stageNum == 4) {
            Tutorial.get().getGameWorld().add(Tutorial.Layer.h_bullet.ordinal(), bullet);
        }
    }

    //플레이어와 아이템간의 충돌 --> 아이템 획득
    private void checkItemCollision() {
        if(stageNum == 1) {
            ArrayList<GameObject> items = Stage_1.get().getGameWorld().objectsAtLayer(Stage_1.Layer.item.ordinal());

            for (GameObject obj : items) {
                if (!(obj instanceof Item)) {
                    continue;
                }
                Item item = (Item) obj;
                if (CollisionHelper.collides(this, item)) {
                    item.remove();
                }
            }

            for (GameObject obj : items) {
                if (!(obj instanceof I_Mask)) {
                    continue;
                }
                I_Mask itemMask = (I_Mask) obj;
                if (CollisionHelper.collides(this, itemMask)) {
                    SoundEffects.get().play(R.raw.eat_item);
                    Stage_1.get().setScoreObject(500);
                    itemMask.remove();
                    if(shieldCnt==0) {
                        shieldCnt++;
                        HShield = new H_Shield(x, y);
                        Stage_1.get().getGameWorld().add(Stage_1.Layer.item.ordinal(), HShield);
                    }
                }
            }

            for (GameObject obj : items) {
                if (!(obj instanceof I_Vaccine)) {
                    continue;
                }
                I_Vaccine itemVaccine = (I_Vaccine) obj;
                if (CollisionHelper.collides(this, itemVaccine)) {
                    SoundEffects.get().play(R.raw.eat_item_2);
                    Stage_1.get().setScoreObject(300);
                    itemVaccine.remove();
                    if(vaccineCnt==0) {
                        vaccineCnt++;
                        leftVaccine = new H_SubVaccine(x, y, this.damage,stageNum);
                        Stage_1.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), leftVaccine);
                        rightVaccine = new H_SubVaccine(x , y, this.damage, stageNum);
                        Stage_1.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), rightVaccine);
                    }
                }
            }
        } else if(stageNum == 2) {
            ArrayList<GameObject> items = Stage_2.get().getGameWorld().objectsAtLayer(Stage_2.Layer.item.ordinal());

            for (GameObject obj : items) {
                if (!(obj instanceof Item)) {
                    continue;
                }
                Item item = (Item) obj;
                if (CollisionHelper.collides(this, item)) {
                    item.remove();
                }
            }

            for (GameObject obj : items) {
                if (!(obj instanceof I_Mask)) {
                    continue;
                }
                I_Mask itemMask = (I_Mask) obj;
                if (CollisionHelper.collides(this, itemMask)) {
                    SoundEffects.get().play(R.raw.eat_item);
                    Stage_2.get().setScoreObject(500);
                    itemMask.remove();
                    if(shieldCnt==0) {
                        shieldCnt++;
                        HShield = new H_Shield(x, y);
                        Stage_2.get().getGameWorld().add(Stage_2.Layer.item.ordinal(), HShield);
                    }
                }
            }

            for (GameObject obj : items) {
                if (!(obj instanceof I_Vaccine)) {
                    continue;
                }
                I_Vaccine itemVaccine = (I_Vaccine) obj;
                if (CollisionHelper.collides(this, itemVaccine)) {
                    SoundEffects.get().play(R.raw.eat_item_2);
                    Stage_2.get().setScoreObject(300);
                    itemVaccine.remove();
                    if(vaccineCnt==0) {
                        vaccineCnt++;
                        leftVaccine = new H_SubVaccine(x, y, this.damage, stageNum);
                        Stage_2.get().getGameWorld().add(Stage_2.Layer.ui.ordinal(), leftVaccine);
                        rightVaccine = new H_SubVaccine(x , y, this.damage, stageNum);
                        Stage_2.get().getGameWorld().add(Stage_2.Layer.ui.ordinal(), rightVaccine);
                    }
                }
            }
        } else if(stageNum == 3) {
            ArrayList<GameObject> items = Stage_3.get().getGameWorld().objectsAtLayer(Stage_3.Layer.item.ordinal());

            for (GameObject obj : items) {
                if (!(obj instanceof Item)) {
                    continue;
                }
                Item item = (Item) obj;
                if (CollisionHelper.collides(this, item)) {
                    item.remove();
                }
            }

            for (GameObject obj : items) {
                if (!(obj instanceof I_Mask)) {
                    continue;
                }
                I_Mask itemMask = (I_Mask) obj;
                if (CollisionHelper.collides(this, itemMask)) {
                    SoundEffects.get().play(R.raw.eat_item);
                    Stage_3.get().setScoreObject(500);
                    itemMask.remove();
                    if(shieldCnt==0) {
                        shieldCnt++;
                        HShield = new H_Shield(x, y);
                        Stage_3.get().getGameWorld().add(Stage_3.Layer.item.ordinal(), HShield);
                    }
                }
            }

            for (GameObject obj : items) {
                if (!(obj instanceof I_Vaccine)) {
                    continue;
                }
                I_Vaccine itemVaccine = (I_Vaccine) obj;
                if (CollisionHelper.collides(this, itemVaccine)) {
                    SoundEffects.get().play(R.raw.eat_item_2);
                    Stage_3.get().setScoreObject(300);
                    itemVaccine.remove();
                    if(vaccineCnt==0) {
                        vaccineCnt++;
                        leftVaccine = new H_SubVaccine(x, y, this.damage, stageNum);
                        Stage_3.get().getGameWorld().add(Stage_3.Layer.ui.ordinal(), leftVaccine);
                        rightVaccine = new H_SubVaccine(x , y, this.damage, stageNum);
                        Stage_3.get().getGameWorld().add(Stage_3.Layer.ui.ordinal(), rightVaccine);
                    }
                }
            }
        }
        else if(stageNum == 4) {
            ArrayList<GameObject> items = Tutorial.get().getGameWorld().objectsAtLayer(Tutorial.Layer.item.ordinal());

            for (GameObject obj : items) {
                if (!(obj instanceof Item)) {
                    continue;
                }
                Item item = (Item) obj;
                if (CollisionHelper.collides(this, item)) {
                    item.remove();
                }
            }

            for (GameObject obj : items) {
                if (!(obj instanceof I_Mask)) {
                    continue;
                }
                I_Mask itemMask = (I_Mask) obj;
                if (CollisionHelper.collides(this, itemMask)) {
                    SoundEffects.get().play(R.raw.eat_item);
                    Tutorial.get().setScoreObject(500);
                    itemMask.remove();
                    if(shieldCnt==0) {
                        shieldCnt++;
                        HShield = new H_Shield(x, y);
                        Tutorial.get().getGameWorld().add(Tutorial.Layer.item.ordinal(), HShield);
                    }
                }
            }

            for (GameObject obj : items) {
                if (!(obj instanceof I_Vaccine)) {
                    continue;
                }
                I_Vaccine itemVaccine = (I_Vaccine) obj;
                if (CollisionHelper.collides(this, itemVaccine)) {
                    SoundEffects.get().play(R.raw.eat_item_2);
                    Tutorial.get().setScoreObject(300);
                    itemVaccine.remove();
                    if(vaccineCnt==0) {
                        vaccineCnt++;
                        leftVaccine = new H_SubVaccine(x, y, this.damage, 4);
                        Tutorial.get().getGameWorld().add(Tutorial.Layer.ui.ordinal(), leftVaccine);
                        rightVaccine = new H_SubVaccine(x , y, this.damage, 4);
                        Tutorial.get().getGameWorld().add(Tutorial.Layer.ui.ordinal(), rightVaccine);
                    }
                }
            }
        }
    }

    //플레이어와 적 객체와의 충돌
    private void checkVirusCollision() {
        if(stageNum == 1) {
            ArrayList<GameObject> enemies = Stage_1.get().getGameWorld().objectsAtLayer(Stage_1.Layer.enemy.ordinal());
            for (GameObject obj : enemies) {
                if (!(obj instanceof E_Virus)) {
                    continue;
                }
                E_Virus EVirus = (E_Virus) obj;
                if (CollisionHelper.collides(this, EVirus)) {
                    EVirus.remove();
                    EVirus.removeUI();
                    if(shieldCnt==0)
                        this.setDamage(EVirus.getDamage());
                    else {
                        HShield.remove();
                        shieldCnt--;
                    }
                }
            }
        } else if(stageNum == 2) {
            ArrayList<GameObject> enemies = Stage_2.get().getGameWorld().objectsAtLayer(Stage_2.Layer.enemy.ordinal());
            for (GameObject obj : enemies) {
                if (!(obj instanceof E_Virus)) {
                    continue;
                }
                E_Virus EVirus = (E_Virus) obj;
                if (CollisionHelper.collides(this, EVirus)) {
                    EVirus.remove();
                    EVirus.removeUI();
                    if(shieldCnt==0)
                        this.setDamage(EVirus.getDamage());
                    else {
                        HShield.remove();
                        shieldCnt--;
                    }
                }
            }
        } else if(stageNum == 3) {
            ArrayList<GameObject> enemies = Stage_3.get().getGameWorld().objectsAtLayer(Stage_3.Layer.enemy.ordinal());
            for (GameObject obj : enemies) {
                if (!(obj instanceof E_Virus)) {
                    continue;
                }
                E_Virus EVirus = (E_Virus) obj;
                if (CollisionHelper.collides(this, EVirus)) {
                    EVirus.remove();
                    EVirus.removeUI();
                    if(shieldCnt==0)
                        this.setDamage(EVirus.getDamage());
                    else {
                        HShield.remove();
                        shieldCnt--;
                    }
                }
            }
        } else if(stageNum == 4) {
            ArrayList<GameObject> enemies = Tutorial.get().getGameWorld().objectsAtLayer(Tutorial.Layer.enemy.ordinal());
            for (GameObject obj : enemies) {
                if (!(obj instanceof E_Virus)) {
                    continue;
                }
                E_Virus EVirus = (E_Virus) obj;
                if (CollisionHelper.collides(this, EVirus)) {
                    EVirus.remove();
                    EVirus.removeUI();
                    if(shieldCnt==0)
                        this.setDamage(EVirus.getDamage());
                    else {
                        HShield.remove();
                        shieldCnt--;
                    }
                }
            }
        }

        if(stageNum == 1) {
            ArrayList<GameObject> enemies = Stage_1.get().getGameWorld().objectsAtLayer(Stage_1.Layer.enemy.ordinal());
            for (GameObject obj : enemies) {
                if (!(obj instanceof E_Mitosis_Virus)) {
                    continue;
                }
                E_Mitosis_Virus EVirus = (E_Mitosis_Virus) obj;
                if (CollisionHelper.collides(this, EVirus)) {
                    EVirus.remove();
                    EVirus.removeUI();
                    if(shieldCnt==0)
                        this.setDamage(EVirus.getDamage());
                    else {
                        HShield.remove();
                        shieldCnt--;
                    }
                }
            }
        } else if(stageNum == 2) {
            ArrayList<GameObject> enemies = Stage_2.get().getGameWorld().objectsAtLayer(Stage_2.Layer.enemy.ordinal());
            for (GameObject obj : enemies) {
                if (!(obj instanceof E_Mitosis_Virus)) {
                    continue;
                }
                E_Mitosis_Virus EVirus = (E_Mitosis_Virus) obj;
                if (CollisionHelper.collides(this, EVirus)) {
                    EVirus.remove();
                    EVirus.removeUI();
                    if(shieldCnt==0)
                        this.setDamage(EVirus.getDamage());
                    else {
                        HShield.remove();
                        shieldCnt--;
                    }
                }
            }
        } else if(stageNum == 3) {
            ArrayList<GameObject> enemies = Stage_3.get().getGameWorld().objectsAtLayer(Stage_3.Layer.enemy.ordinal());
            for (GameObject obj : enemies) {
                if (!(obj instanceof E_Mitosis_Virus)) {
                    continue;
                }
                E_Mitosis_Virus EVirus = (E_Mitosis_Virus) obj;
                if (CollisionHelper.collides(this, EVirus)) {
                    EVirus.remove();
                    EVirus.removeUI();
                    if(shieldCnt==0)
                        this.setDamage(EVirus.getDamage());
                    else {
                        HShield.remove();
                        shieldCnt--;
                    }
                }
            }
        }
        else if(stageNum == 4) {
            ArrayList<GameObject> enemies = Tutorial.get().getGameWorld().objectsAtLayer(Tutorial.Layer.enemy.ordinal());
            for (GameObject obj : enemies) {
                if (!(obj instanceof E_Mitosis_Virus)) {
                    continue;
                }
                E_Mitosis_Virus EVirus = (E_Mitosis_Virus) obj;
                if (CollisionHelper.collides(this, EVirus)) {
                    EVirus.remove();
                    EVirus.removeUI();
                    if(shieldCnt==0)
                        this.setDamage(EVirus.getDamage());
                    else {
                        HShield.remove();
                        shieldCnt--;
                    }
                }
            }
        }

        if(stageNum == 3) {
            ArrayList<GameObject> enemies = Stage_3.get().getGameWorld().objectsAtLayer(Stage_3.Layer.enemy.ordinal());
            for (GameObject obj : enemies) {
                if (!(obj instanceof E_Mitosis_Virus)) {
                    continue;
                }
                E_Mitosis_Virus EVirus = (E_Mitosis_Virus) obj;
                if (CollisionHelper.collides(this, EVirus)) {
                    EVirus.remove();
                    EVirus.removeUI();
                    if(shieldCnt==0)
                        this.setDamage(EVirus.getDamage());
                    else {
                        HShield.remove();
                        shieldCnt--;
                    }
                }
                if (!(obj instanceof E_Guided_Virus)) {
                    continue;
                }
                E_Guided_Virus EGuided_Virus = (E_Guided_Virus) obj;
                if (CollisionHelper.collides(this, EGuided_Virus)) {
                    EGuided_Virus.remove();
                    EGuided_Virus.removeUI();
                    if(shieldCnt==0)
                        this.setDamage(EGuided_Virus.getDamage());
                    else {
                        HShield.remove();
                        shieldCnt--;
                    }
                }
            }
        }
    }

    private void checkBulletCollision() {
        if(stageNum == 1) {
            ArrayList<GameObject> bullets = Stage_1.get().getGameWorld().objectsAtLayer(Stage_1.Layer.e_bullet.ordinal());
            for (GameObject obj : bullets) {
                if (!(obj instanceof Bullet)) {
                    continue;
                }
                Bullet enemybullet = (Bullet) obj;
                if (CollisionHelper.collides(this, enemybullet)) {
                    enemybullet.remove();
                    if(shieldCnt==0)
                        this.setDamage(1);
                    else {
                        HShield.remove();
                        shieldCnt--;
                    }
                }
            }
        } else if(stageNum == 2) {
            ArrayList<GameObject> bullets = Stage_2.get().getGameWorld().objectsAtLayer(Stage_2.Layer.e_bullet.ordinal());
            for (GameObject obj : bullets) {
                if (!(obj instanceof Bullet)) {
                    continue;
                }
                Bullet enemybullet = (Bullet) obj;
                if (CollisionHelper.collides(this, enemybullet)) {
                    enemybullet.remove();
                    if(shieldCnt==0)
                        this.setDamage(1);
                    else {
                        HShield.remove();
                        shieldCnt--;
                    }
                }
            }
        } else if(stageNum == 3) {
            ArrayList<GameObject> bullets = Stage_3.get().getGameWorld().objectsAtLayer(Stage_3.Layer.e_bullet.ordinal());
            for (GameObject obj : bullets) {
                if (!(obj instanceof Bullet)) {
                    continue;
                }
                Bullet enemybullet = (Bullet) obj;
                if (CollisionHelper.collides(this, enemybullet)) {
                    enemybullet.remove();
                    if(shieldCnt==0)
                        this.setDamage(1);
                    else {
                        HShield.remove();
                        shieldCnt--;
                    }
                }
            }
        }else if(stageNum == 4) {
            ArrayList<GameObject> bullets = Tutorial.get().getGameWorld().objectsAtLayer(Tutorial.Layer.e_bullet.ordinal());
            for (GameObject obj : bullets) {
                if (!(obj instanceof Bullet)) {
                    continue;
                }
                Bullet enemybullet = (Bullet) obj;
                if (CollisionHelper.collides(this, enemybullet)) {
                    enemybullet.remove();
                    if(shieldCnt==0)
                        this.setDamage(1);
                    else {
                        HShield.remove();
                        shieldCnt--;
                    }
                }
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        xPos = event.getX();
        yPos = event.getY();
        if(yPos > UiBridge.metrics.size.y/2) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    xDown = event.getX();
                    yDown = event.getY();
//                    Log.d(TAG,"x : "+xDown + " y :"+yDown);
//                    if(xDown<300 && yDown<500) {
                    down = true;
//                    }
                    break;

                case MotionEvent.ACTION_MOVE:
                    float dx = event.getX() - xDown;
                    float dy = event.getY() - yDown;
                    this.angle = (float) Math.atan2(dy, dx);//*(180/Math.PI);
                    break;
                default:
                    down = false;
            }
            return true;
        }
        else{ return false;}
    }

    public int getHorzDirection() {
        if (!down) return 0;
        int dir = angle < Math.PI / 2 && angle > -Math.PI / 2 ? 1 : -1;
        return dir;
    }

    @Override
    public void getBox(RectF rect) {
        int hw = width / 2;
        int hh = height / 2;
        rect.left = x - hw;
        rect.top = y - hh;
        rect.right = x + hw;
        rect.bottom = y + hh;
    }
}
