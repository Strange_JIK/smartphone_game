package kr.ac.kpu.game.scgyong.gameskeleton.game.scene;

import android.media.MediaPlayer;
import android.util.Log;
import android.widget.BaseAdapter;

import java.util.Random;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameScene;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameTimer;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.UiBridge;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.BitmapObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.ScoreObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.ui.Button;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.enemy_generator.E_Mitosis_Virus;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.enemy_generator.E_Stage2_Boss;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.enemy_generator.E_Virus;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator.H_Vaccine;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator.I_Mask;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator.I_Vaccine;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.stage_obj.CellBackGround;
import kr.ac.kpu.game.scgyong.gameskeleton.ui.activity.GameActivity;

public class Stage_2 extends GameScene {
    private static final String TAG = Start_Menu.class.getSimpleName();
    public static final long DAFAULTTIME=100_000_0000;
    private static final int STAGE_NUM = 2;
    private static Stage_2 instance;
    private E_Stage2_Boss boss;
    private H_Vaccine H_Vaccine;
    private long lastPop;
    private int spawnCount = 0;
    private boolean bBossSpawn = false, BossDead = false;
    private BitmapObject game_stage,boss_alarm;
    private float stagemove_x=0,stagemove_y=0;
    private float alarmmove_x=0,alarmmove_y=0;
    private long entercount=0;
    public boolean pause_switch =true;
    private long pausetime=0;
    private BitmapObject score;
    private GameActivity activity;
    private BaseAdapter adapter;
    private MediaPlayer mp;
    private boolean musicon;

    public enum Layer {
        bg, h_bullet,e_bullet,enemy, player , item, ui, COUNT,
    }

    private ScoreObject scoreObject;
    private GameTimer timer;

    public void musicstart(){

        mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener(){
            @Override public void onPrepared(MediaPlayer player) {
                player.start();
                player.setLooping(true);
            }
        });
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener(){
            @Override public void onCompletion(MediaPlayer player) {
                if(mp!=null)
                {
                    mp.release(); mp = null;
                }
            }
        });
    }
    @Override
    protected int getLayerCount() {
        return Layer.COUNT.ordinal();
    }
    public void setActivity(GameActivity activity) {
        this.activity = activity;
    }

    public void setAdapter(BaseAdapter adapter) {
        this.adapter = adapter;
    }
    public GameActivity getActivity() {
        return this.activity;
    }
    public void setScore(ScoreObject scoreObject) {
        this.scoreObject = scoreObject;
    }
    private void initObjects() {
        gameWorld.add(Layer.bg.ordinal(), new CellBackGround());

        //UI
        //게임 스테이지
        game_stage = new BitmapObject(UiBridge.metrics.size.x / 2,UiBridge.metrics.size.y ,500,300,R.mipmap.stage_2);
        gameWorld.add(Layer.ui.ordinal(), game_stage);

        boss_alarm = new  BitmapObject(UiBridge.metrics.size.x / 2,-50 ,500,0,R.mipmap.warning);
        gameWorld.add(Layer.ui.ordinal(), boss_alarm);

        Button button_OPT = new Button(UiBridge.metrics.size.x - 80, 80, R.mipmap.btn_option, R.mipmap.blue_round_btn, R.mipmap.red_round_btn);
        button_OPT.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
               OptionScene Optscene = new OptionScene();
                Optscene.setActivity(activity);
               Optscene.push();
            }
        });
        gameWorld.add(Layer.ui.ordinal(), button_OPT);


        gameWorld.add(Layer.ui.ordinal(), scoreObject);
        timer = new GameTimer(0.1f, 1);

        //OBJ
        H_Vaccine = new H_Vaccine(UiBridge.metrics.size.x / 2, UiBridge.metrics.size.y - 300, STAGE_NUM);

        H_Vaccine.setActivity(this.activity);
        H_Vaccine.setAdapter(this.adapter);
        gameWorld.add(Layer.player.ordinal(), H_Vaccine);

        ///////bgm
        mp=  MediaPlayer.create(activity,R.raw.stage_2);
        musicstart();
        ////////////////

    }

    public int getScoreValue(){return scoreObject.getScoreValue();}
    @Override
    public void update() {
            super.update();
            long now = GameTimer.getCurrentTimeNanos();
            long elapsed = now - lastPop;
            long stage_time = now - entercount + pausetime;
//            Log.v(TAG, "time :" + stage_time);
            H_Vaccine.SetGameTime(stage_time); //주인공 오브젝트에도 게임시간 넘겨쥼
            //2초동안 스테이지 문구 등장
            if (stage_time < DAFAULTTIME * 2) {
                H_Vaccine.StopFIreSwitch(false);
                if (stagemove_y > -35) {
                    stagemove_y -= 0.5;
                    game_stage.move(stagemove_x, stagemove_y);
                }
            }
            //게임시작 6초부터 스테이지 문구 삭제및 게임시작.
            else if (stage_time > DAFAULTTIME * 6 &&stage_time < DAFAULTTIME * 13) {
                H_Vaccine.StopFIreSwitch(true);
                if (stagemove_y > -50) {
                    stagemove_y -= 0.5;
                    game_stage.move(stagemove_x, stagemove_y);
                    if (stagemove_y <= -70) {
                        stagemove_y = 0;
                        game_stage.remove();
                    }
                }
                if (elapsed > DAFAULTTIME *3) {
                    maskDrop();
                    VaccineDrop();
                    MitosisSpawn();
                    if (elapsed > DAFAULTTIME) {
                        if (!bBossSpawn) {
                            VirusSpawn();

                        }
                    }
                    lastPop = now;
                }

            }
            //16초 이후 등장 문구
            else if(stage_time > DAFAULTTIME * 16 &&stage_time < DAFAULTTIME *22) {
                H_Vaccine.StopFIreSwitch(false);
                stagemove_y=UiBridge.metrics.size.y/2;
                boss_alarm.replace_y(stagemove_y);
                ///////bgm
                if(!musicon){
                    if(mp.isPlaying()){mp.release();}
                    mp = MediaPlayer.create(activity,R.raw.boss_thema);
                    musicstart();
                    musicon=true;
                    ////////////////
                }
            }

            //22초 보스 등장
            else if(stage_time > DAFAULTTIME * 22 ) {
                H_Vaccine.StopFIreSwitch(true);
                boss_alarm.remove();
                if (!bBossSpawn) {
                    bBossSpawn = true;
                    BossSpawn();
                }
            }
        if (timer.done()) {
            scoreObject.add(10);
            timer.reset();
        }


    }
    public void setScoreObject(int score)
    {
        scoreObject.add(score);
    }
    private  void BossSpawn(){
        boss = new E_Stage2_Boss(500,200,100,100);
        gameWorld.add(Layer.enemy.ordinal(),boss);
    }
    public void SetBossStatus(boolean BBossSpawn){
        BossDead = BBossSpawn;
        if(BossDead){
            Log.d(TAG,"STAGEEND");
            Stage_3 scene = new Stage_3();
            scene.setActivity(activity);
            scene.setAdapter(adapter);
            scene.setScore(scoreObject);
            scene.replace();
            mp.release();
        }
    }
    private void VirusSpawn() {
        for(int i=0;i<7;i++) {
            gameWorld.add(Tutorial.Layer.enemy.ordinal(), new E_Virus(100+i*150, 50, 0, 200, STAGE_NUM));
        }
    }
    private void MitosisSpawn() {
        spawnCount++;
        Random r = new Random();
        float min = 0, max = UiBridge.metrics.size.x;
        float random = min + r.nextFloat() * (max - min);
        gameWorld.add(Stage_1.Layer.enemy.ordinal(), new E_Mitosis_Virus(random, 50, 100, 100,STAGE_NUM));
    }
    private void maskDrop() {
        Random r = new Random();
        float min = 0, max = UiBridge.metrics.size.x;
        float random = min + r.nextFloat() * (max - min);
        gameWorld.add(Layer.item.ordinal(), new I_Mask(random, 50, 100, 100, STAGE_NUM));
    }

    private void VaccineDrop() {
        Random r = new Random();
        float min = 0, max = UiBridge.metrics.size.x;
        float random = min + r.nextFloat() * (max - min);
        gameWorld.add(Layer.item.ordinal(), new I_Vaccine(random, 50, 100, 100, STAGE_NUM));
    }


    @Override
    public void enter() {
        super.enter();
        entercount= GameTimer.getCurrentTimeNanos();
        instance = this;
        Log.d(TAG,"-------------------------------enter---------------------------------------");
        initObjects();
    }

    @Override
    public void exit() {
        super.exit();
        mp.release();
        musicon=false;
    }

    public static Stage_2 get() {
        return instance;
    }


}
