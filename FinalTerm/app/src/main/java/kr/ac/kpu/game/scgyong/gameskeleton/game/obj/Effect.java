package kr.ac.kpu.game.scgyong.gameskeleton.game.obj;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameWorld;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.AnimObject;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_1;

public class Effect extends AnimObject {
    private final int effect_type;
    private final int currentCount;

    //x 좌표, y 좌표, 리소스 id, x방향 속도, y방향 속도, 데미지, 내총알/적총알
    public Effect(float x, float y, int width, int height, int bullet_id, int fps, int count, int effect_type) {
        super(x, y, width, height, bullet_id, fps, count);
        this.effect_type = effect_type;
        this.currentCount = count;

    }
    public int getEffectYype() {
        return effect_type;
    }

    @Override
    public float getRadius() {
        return this.width / 4;
    }

    public void update() {
        if(currentCount -1 == getCount())
            remove();
    }
}
