package kr.ac.kpu.game.scgyong.gameskeleton.game.scene;

import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameScene;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.UiBridge;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.BitmapObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.ui.Button;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Temp.HighScoreItem;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Temp.Serializer;
import kr.ac.kpu.game.scgyong.gameskeleton.ui.activity.GameActivity;

public class GameClearScene extends GameScene {
    private static final String TAG = GameClearScene.class.getSimpleName();
    private GameActivity activity;
    private View view;
    private ListView listView;
    private ArrayList<HighScoreItem> scores = new ArrayList<>();
    private int scoreValue;
    private BaseAdapter adapter;

    public void setScore(int scoreValue) {
        this.scoreValue = scoreValue;
    }

    public void setAdapter(BaseAdapter adapter) {
        this.adapter = adapter;
    }

    public enum Layer {
        bg, enemy, player, ui, COUNT
    }

    @Override
    protected int getLayerCount() {
        return Layer.COUNT.ordinal();
    }
    public void setActivity(GameActivity activity) {
        this.activity = activity;
    }

    public void setView(View view)
    {
        this.view = view;
    }
    @Override
    public void enter() {
        super.enter();
        setTransparent(true);
        initObjects();
    }

    private void initObjects() {
        int screenWidth = UiBridge.metrics.size.x;
        int screenHeight = UiBridge.metrics.size.y;

        int cx = UiBridge.metrics.center.x;
        int y = UiBridge.metrics.center.y/2;

        gameWorld.add(Layer.bg.ordinal(), new BitmapObject(UiBridge.metrics.center.x, UiBridge.metrics.center.y, screenWidth, screenHeight, R.mipmap.black_transparent));

        BitmapObject GameOver = new BitmapObject(cx ,y ,256*3,128*3, R.mipmap.game_clear);
        gameWorld.add(Layer.ui.ordinal(), GameOver);

////////////////////////
        y += UiBridge.y(150);
        Button btn_restart = new Button(cx, y, R.mipmap.retry, R.mipmap.blue_round_btn, R.mipmap.red_round_btn);
        btn_restart.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                pop();
                Stage_1 scene = new Stage_1();
                scene.setActivity(activity);
                scene.setAdapter(adapter);
                scene.replace();

            }
        });
        gameWorld.add(Layer.ui.ordinal(), btn_restart);
        y += UiBridge.y(60);
        Button btn_exit = new Button(cx, y, R.mipmap.exit, R.mipmap.blue_round_btn, R.mipmap.red_round_btn);
        btn_exit.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
              pop();
              pop();
            }
        });
        gameWorld.add(Layer.ui.ordinal(), btn_exit);

        y += UiBridge.y(60);
        Button button_score = new Button(cx, y, R.mipmap.rank, R.mipmap.blue_round_btn, R.mipmap.red_round_btn);
        button_score.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
                activity.setContentView(R.layout.activity_rank);
                activity.setScore(scoreValue);
                scores = Serializer.load(activity);
                listView = activity.findViewById(R.id.listview);
                listView.setAdapter(adapter);
            }
        });
        gameWorld.add(Start_Menu.Layer.ui.ordinal(), button_score);
    }
}
