package kr.ac.kpu.game.scgyong.gameskeleton.game.obj.stage_obj;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.bg.ImageScrollBackground;

public class MenuBackground extends ImageScrollBackground {
    public MenuBackground() {
        super(R.mipmap.mainmenu, Orientation.vertical, 0);
    }
}
