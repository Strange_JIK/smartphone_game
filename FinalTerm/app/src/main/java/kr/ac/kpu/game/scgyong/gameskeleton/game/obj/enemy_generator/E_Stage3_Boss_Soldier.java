package kr.ac.kpu.game.scgyong.gameskeleton.game.obj.enemy_generator;

import android.graphics.RectF;

import java.util.ArrayList;
import java.util.Random;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.iface.BoxCollidable;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.iface.LivingObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameTimer;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.UiBridge;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.AnimObject;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Bullet;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Effect;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator.H_Vaccine;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_3;

public class E_Stage3_Boss_Soldier extends AnimObject implements BoxCollidable, LivingObject {
    private static final String TAG = E_Stage3_Boss_Soldier.class.getSimpleName();
    private static final int SPEED = 400;
    private static final float BULLET_SPEED = 5;
    public static final long DAFAULTTIME=100_000_0000;
    protected float dx, dy;
    private int damage;
    private int life;
    private long lastFire;
    private int xdir=1;
    private float x_angle, y_angle;
    private int count =0;
    private float B_xPos =0,B_yPos =0;
    private int angle;
    private boolean phase=false;

    public E_Stage3_Boss_Soldier(float x,float y,float dx,float dy) {
        super(x, y, 0, 0, R.mipmap.eye, 12, 1);
        this.dx = dx;
        this.dy = dy;
        this.life = 30;
        this.damage = 1;
    }
    @Override
    public void update() {

        //원을 돌면서 회전
        B_xPos= 20 * (float)Math.cos(angle * Math.PI/180)+x;
        B_yPos= 20 *(float)Math.sin(angle* Math.PI/180)+y;
        x=B_xPos;
        y=B_yPos;
        angle+=10;

        if(phase){
            ArrayList<GameObject> heros = Stage_3.get().getGameWorld().objectsAtLayer(Stage_3.Layer.player.ordinal());
            float heroposX = heros.get(0).getX();
            float heroposY = heros.get(0).getY();
            float x_dest =  heroposX - x;
            float y_dest =  heroposY - y;
            double rad = Math.atan2(y_dest, x_dest);
            //총알 패턴
            long now = GameTimer.getCurrentTimeNanos();
            long elapsed = now - lastFire;

                //플레이어를 따라 발사
                if (elapsed > DAFAULTTIME *0.1) {
                    if(count>10 && count <=20)
                    {
                        count+=1;
                        lastFire = now;
                    }
                    else if(count>20)
                    {
                        lastFire = now;
                        count=0;
                    }
                    else {
                        x_angle = BULLET_SPEED * (float) Math.cos(rad);
                        y_angle = BULLET_SPEED * (float) Math.sin(rad);
                        fire();
                        lastFire = now;
                        count+=1;
                    }

                }

        }
    }

    //    @Override
//    public boolean onTouchEvent(MotionEvent e) {
//
//    }
    public void check_phase(boolean phase2){
        phase = phase2;
    }
    @Override
    public void getBox(RectF rect) {
        int hw = width / 2;
        int hh = height / 2;
        rect.left = x - hw;
        rect.top = y - hh;
        rect.right = x + hw;
        rect.bottom = y + hh;
    }


    private void fire() {
        Bullet bullet = new Bullet(x, y,100,100, R.mipmap.temp_bullet_6, 200*x_angle, 200*y_angle,damage,false, 3);
        //dx x축 속도 , dy y축 속도
        Stage_3.get().getGameWorld().add(Stage_3.Layer.e_bullet.ordinal(), bullet);
    }

    @Override
    public int getLife() {

        return life;
    }

    @Override
    public void setLife(float life) {
        this.life += life;
    }

    @Override
    public int getDamage() {
        return damage;
    }

    @Override
    public void setDamage(int damage) {
        this.life -= damage;
        if(this.life == 0) {
            remove();
            Effect effect = new Effect(x, y, 200, 200, R.mipmap.dead6, 40, 16, 1);
            Stage_3.get().getGameWorld().add(Stage_3.Layer.ui.ordinal(), effect);
            Stage_3.get().setScoreObject(1000);
            Stage_3.get().SetBossStatus(true);
        }
    }
}
