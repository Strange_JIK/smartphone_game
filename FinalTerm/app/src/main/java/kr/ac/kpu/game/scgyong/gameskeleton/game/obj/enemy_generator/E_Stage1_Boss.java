package kr.ac.kpu.game.scgyong.gameskeleton.game.obj.enemy_generator;

import android.graphics.RectF;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.iface.BoxCollidable;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.iface.LivingObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameTimer;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.UiBridge;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.AnimObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Bullet;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Effect;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator.UI_Bar;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator.UI_HP;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_1;

public class E_Stage1_Boss extends AnimObject implements BoxCollidable, LivingObject {


    private static final String TAG = E_Stage1_Boss.class.getSimpleName();
    private static final int SPEED = 400;
    private static final float BULLET_SPEED = 5;
    public static final long DAFAULTTIME=100_000_0000;
    private final int maxlife;
    private final UI_Bar bar;
    private final UI_HP hp;
    protected float dx, dy;
    private int damage;
    private int life;
    private long lastFire;
    private int xdir=1;
    private float x_angle, y_angle;
    private int angle_count =0;

    public E_Stage1_Boss(float x, float y, float dx, float dy) {
        super(x, y, 0, 0, R.mipmap.boss_1, 12, 6);
        this.dx = dx;
        this.dy = dy;
        life = 30;
        maxlife=30;
        this.damage = 1;
        bar = new UI_Bar(0, 10);
        hp = new UI_HP(0, 10,getLife(),maxlife);
        bar.setWidth(1200);
        bar.setHeight(30);
        hp.setWidth(1200);
        hp.setHeight(30);
        Stage_1.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), hp);
        Stage_1.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), bar);
    }
    public void removeUI(){
        hp.remove();
        bar.remove();}
    @Override
    public void update() {
     //체력바
        bar.setPos(10,10);
        hp.setPos(10,10,life);
    //총알 패턴
        long now = GameTimer.getCurrentTimeNanos();
        long elapsed = now - lastFire;
        if(life>15) {

            if (elapsed > DAFAULTTIME * 1) {
//            //패턴 1 둥글게 터트리기(1초마다 발사)
                for (int i = 0; i < 360; i += 10) {
                    x_angle = BULLET_SPEED * (float) Math.cos(i);
                    y_angle = BULLET_SPEED * (float) Math.sin(i);
                    fire();
                    lastFire = now;
                }
            }
        }
        else {
            if (elapsed > DAFAULTTIME * 0.01) {
//          패턴 2 한발씩 둥글게 (0.1초마다 쏨)

                x_angle = BULLET_SPEED * (float) Math.cos(angle_count);
                y_angle = BULLET_SPEED * (float) Math.sin(angle_count);
                if (angle_count < 360) angle_count += 10;
                else angle_count = 0;
                fire();
                lastFire = now;

            }
        }
    //보스 좌우 이동
        x += xdir*SPEED* GameTimer.getTimeDiffSeconds();
        if(x < 0) {
            x=0;
            xdir =1;
        }
        else if(x> UiBridge.metrics.size.x) {
            x = UiBridge.metrics.size.x;
            xdir = -1;
        }
    }

//    @Override
//    public boolean onTouchEvent(MotionEvent e) {
//
//    }
    @Override
    public void getBox(RectF rect) {
        int hw = width / 2;
        int hh = height / 2;
        rect.left = x - hw;
        rect.top = y - hh;
        rect.right = x + hw;
        rect.bottom = y + hh;
    }


    private void fire() {
        Bullet bullet  = new Bullet(x,y,50,50,R.mipmap.temp_bullet_6,30*x_angle,30*+y_angle,damage,false,1);
        //dx x축 속도 , dy y축 속도
        Stage_1.get().getGameWorld().add(Stage_1.Layer.e_bullet.ordinal(), bullet);
    }

    @Override
    public int getLife() {

        return life;
    }

    @Override
    public void setLife(float life) {
        this.life += life;
    }

    @Override
    public int getDamage() {
        return damage;
    }

    @Override
    public void setDamage(int damage) {
        this.life -= damage;
        SoundEffects.get().play(R.raw.die_sound);
        if(this.life <= 0) {
            remove();
            removeUI();
            Effect effect = new Effect(x, y, 200, 200, R.mipmap.dead6, 40, 16, 1);
            Stage_1.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), effect);
            Stage_1.get().setScoreObject(2000);
            Stage_1.get().SetBossStatus(true);
        }
    }
}
