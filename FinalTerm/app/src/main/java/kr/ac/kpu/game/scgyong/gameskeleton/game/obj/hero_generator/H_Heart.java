package kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.BitmapObject;

public class H_Heart extends BitmapObject  {

    public H_Heart(float x, float y) {
        super(x, y, 70,70 , R.mipmap.heart1);
    }

    @Override
    public float getRadius() {
        return this.width / 4;
    }
    public void update() {}

}
