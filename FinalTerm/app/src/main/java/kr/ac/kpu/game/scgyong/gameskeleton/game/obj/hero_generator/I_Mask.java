package kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator;

import android.graphics.RectF;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.iface.BoxCollidable;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameTimer;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.UiBridge;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.BitmapObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.res.sound.SoundEffects;

public class I_Mask extends BitmapObject implements BoxCollidable {
    private static final float ITEM_DROP_SPEED = 3;
    protected float dx, dy;

    public I_Mask(float x, float y, float dx, float dy, int stageNum) {
        super(x, y, 57*4,34*4 , R.mipmap.mask_item_1);
        this.dx = dx;
        this.dy = dy;
    }

    @Override
    public float getRadius() {
        return this.width / 4;
    }

    public void update() {
        float seconds = GameTimer.getTimeDiffSeconds();
        float radius = getRadius();
        int screenHeight = UiBridge.metrics.size.y;
        y += dy * seconds * ITEM_DROP_SPEED;
        if (dy > 0 && y > screenHeight + radius) {
            remove();
        }
    }

    @Override
    public void getBox(RectF rect) {
        int hw = width / 2;
        int hh = height / 2;
        rect.left = x - hw;
        rect.top = y - hh;
        rect.right = x + hw;
        rect.bottom = y + hh;
    }

}
