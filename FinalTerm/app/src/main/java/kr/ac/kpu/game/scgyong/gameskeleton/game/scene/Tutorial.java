package kr.ac.kpu.game.scgyong.gameskeleton.game.scene;

import android.graphics.RectF;
import android.util.Log;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameScene;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameTimer;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.UiBridge;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.BitmapObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.ScoreObject;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.enemy_generator.E_Virus;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator.H_Vaccine;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator.I_Mask;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator.I_Vaccine;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.stage_obj.CellBackGround;
import kr.ac.kpu.game.scgyong.gameskeleton.ui.activity.GameActivity;

public class Tutorial extends GameScene {
    private static final String TAG = Start_Menu.class.getSimpleName();
    public static final long DEFAULTTIME =100_000_0000;
    public static final long SPAWNTIME = 100_000_0000*2;

    private static final int STAGE_NUM = 4;
    private static Tutorial instance;
    private H_Vaccine H_Vaccine;
    private long lastPop;
    private BitmapObject game_stage,boss_alarm;
    private float stagemove_x=0,stagemove_y=0;
    private float alarmmove_x=0,alarmmove_y=0;
    private long entercount=0;
    public boolean pause_switch =true;
    private long pausetime=0;
    private BitmapObject score;
    private boolean bBossSpawn;
    private BitmapObject tuto_start;
    private BitmapObject tuto_attack;
    private BitmapObject tuto_move;
    private BitmapObject tooltip_vaccine;
    private BitmapObject tooltip_mask;
    private boolean bVirusSpawn = false;
    private boolean bMaskSpawn = false;
    private boolean bVaccineSpawn = false;
    private boolean bMaskDropOnce = false;
    private boolean bVaccineDropOnce = false;
    private BitmapObject tuto_end;
    private GameActivity activity;


    public enum Layer {
        bg, h_bullet,e_bullet,enemy, player , item, ui, COUNT,
    }

    private ScoreObject scoreObject;
    private GameTimer timer;
    public void setActivity(GameActivity activity) {
        this.activity = activity;
    }
    @Override
    protected int getLayerCount() {
        return Layer.COUNT.ordinal();
    }

    private void initObjects() {
        gameWorld.add(Layer.bg.ordinal(), new CellBackGround());

        //UI
        //게임 스테이지
        game_stage = new BitmapObject(UiBridge.metrics.size.x / 2,UiBridge.metrics.size.y,500,300,R.mipmap.tutorial);
        gameWorld.add(Layer.ui.ordinal(), game_stage);


        tuto_start = new BitmapObject(UiBridge.metrics.size.x / 2,UiBridge.metrics.size.y/2 ,586,365,R.mipmap.tuto_start);


        tuto_attack = new BitmapObject(UiBridge.metrics.size.x / 2,UiBridge.metrics.size.y/2 ,586,364,R.mipmap.tuto_attack);

        tuto_move = new BitmapObject(UiBridge.metrics.size.x / 2,UiBridge.metrics.size.y/2 ,733,594,R.mipmap.tuto_move);
        tuto_end = new BitmapObject(UiBridge.metrics.size.x / 2,UiBridge.metrics.size.y/2 ,566,370,R.mipmap.tuto_end);

        tooltip_vaccine = new BitmapObject(UiBridge.metrics.size.x / 2,UiBridge.metrics.size.y/2 ,193*2,264*2,R.mipmap.tooltip_vaccine);

        tooltip_mask = new BitmapObject(UiBridge.metrics.size.x / 2,UiBridge.metrics.size.y/2 ,225*2,200*2,R.mipmap.tooltip_mask);



        score = new BitmapObject(140,100 ,245,70,R.mipmap.score1);
        gameWorld.add(Layer.ui.ordinal(), score);
        RectF rbox = new RectF(500, 60, 570, 140);
        scoreObject = new ScoreObject(R.mipmap.number3, rbox);
        gameWorld.add(Layer.ui.ordinal(), scoreObject);
        timer = new GameTimer(2, 1);

        //OBJ
        H_Vaccine = new H_Vaccine(UiBridge.metrics.size.x / 2, UiBridge.metrics.size.y - 300,STAGE_NUM);
        gameWorld.add(Layer.player.ordinal(), H_Vaccine);
    }

    @Override
    public void update() {
            super.update();
            long now = GameTimer.getCurrentTimeNanos();
            long elapsed = now - lastPop;
            long stage_time = now - entercount + pausetime;
//            Log.v(TAG, "time :" + stage_time);
            H_Vaccine.SetGameTime(stage_time); //주인공 오브젝트에도 게임시간 넘겨쥼
            //2초동안 스테이지 문구 등장
            if (stage_time < DEFAULTTIME * 2) {
                H_Vaccine.StopFIreSwitch(false);
                if (stagemove_y > -35) {
                    stagemove_y -= 0.5;
                    game_stage.move(stagemove_x, stagemove_y);
                }
            }
            //게임시작 6초부터 스테이지 문구 삭제및 게임시작.
            else if (stage_time > DEFAULTTIME * 6 &&stage_time < DEFAULTTIME * 13) {
                H_Vaccine.StopFIreSwitch(true);
                if (stagemove_y > -50) {
                    stagemove_y -= 0.5;
                    game_stage.move(stagemove_x, stagemove_y);
                    if (stagemove_y <= -70) {
                        stagemove_y = 0;
                        game_stage.remove();
                    }
                }
            }
            //시작하고 1초뒤에
            else if(stage_time>DEFAULTTIME*2&&stage_time<DEFAULTTIME*4)
            {
                gameWorld.add(Layer.ui.ordinal(), tuto_start);
            }
            else if(stage_time>DEFAULTTIME*4&&stage_time<DEFAULTTIME*10)
            {
                tuto_start.remove();
                gameWorld.add(Layer.ui.ordinal(), tuto_move);
            }
            else if(stage_time>DEFAULTTIME*10&&stage_time<DEFAULTTIME*14)
            {
                tuto_move.remove();
                gameWorld.add(Layer.ui.ordinal(), tuto_attack);
            }
            else if(stage_time>DEFAULTTIME*14&&stage_time<DEFAULTTIME*15)
            {
                tuto_attack.remove();
                bVirusSpawn = true;
            }
            else if(stage_time>DEFAULTTIME*24&&stage_time<DEFAULTTIME*26)
            {
                gameWorld.add(Layer.ui.ordinal(), tooltip_mask);
            }
            else if(stage_time>DEFAULTTIME*26&&stage_time<DEFAULTTIME*29)
            {
                tooltip_mask.remove();
                bMaskSpawn = true;
                H_Vaccine.StopFIreSwitch(false);
            }
            else if(stage_time>DEFAULTTIME*29&&stage_time<DEFAULTTIME*31)
            {
                gameWorld.add(Layer.ui.ordinal(), tooltip_vaccine);
            }
            else if(stage_time>DEFAULTTIME*31&&stage_time<DEFAULTTIME*34) {
                tooltip_vaccine.remove();
                bVaccineSpawn = true;
                H_Vaccine.StopFIreSwitch(true);
            }
            else if(stage_time>DEFAULTTIME*40&&stage_time<DEFAULTTIME*43)
            {
                gameWorld.add(Layer.ui.ordinal(), tuto_end);
            }
            else if(stage_time>DEFAULTTIME*43)
            {
                tuto_end.remove();
                pop();
            }


        if(bVirusSpawn) {
                if (elapsed > SPAWNTIME) {
                    if (!bBossSpawn) {
                        VirusSpawn();
                    }
                    lastPop = now;
                }
            }
        if(bMaskSpawn&&!bMaskDropOnce) {
            maskDrop();
            bMaskDropOnce = true;
        }
        if(bVaccineSpawn&&!bVaccineDropOnce) {
            VaccineDrop();
            bVaccineDropOnce = true;
        }



    }
    public void setScoreObject(int score)
    {
        scoreObject.add(score);
    }

    private void VirusSpawn() {
        for(int i=0;i<7;i++) {
            gameWorld.add(Layer.enemy.ordinal(), new E_Virus(100+i*150, 50, 0, 100, STAGE_NUM));
        }
    }

    private void maskDrop() {
        gameWorld.add(Layer.item.ordinal(), new I_Mask(UiBridge.metrics.size.x/2, 50, 0, 130,STAGE_NUM));
    }

    private void VaccineDrop() {
        gameWorld.add(Layer.item.ordinal(), new I_Vaccine(UiBridge.metrics.size.x/2, 50, 0, 130,STAGE_NUM));
    }


    @Override
    public void enter() {
        super.enter();
        entercount= GameTimer.getCurrentTimeNanos();
        instance = this;
        Log.d(TAG,"-------------------------------enter---------------------------------------");
        initObjects();
    }
    public static Tutorial get() {
        return instance;
    }


}
