package kr.ac.kpu.game.scgyong.gameskeleton.game.obj.enemy_generator;

import android.graphics.RectF;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.iface.BoxCollidable;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.iface.LivingObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameTimer;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.UiBridge;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.AnimObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.res.bitmap.FrameAnimationBitmap;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Effect;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator.UI_Bar;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator.UI_HP;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_1;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_2;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_3;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Tutorial;

public class E_Virus extends AnimObject implements BoxCollidable, LivingObject {

    private static final String TAG = E_Virus.class.getSimpleName();
    private static final float VIRUS_DROP_SPPED = 2;
    private final int stageNum;
    private final int maxlife;
    private final UI_Bar bar;
    private final UI_HP hp;
    protected float dx, dy;
    public static final int BULLET_FIRE_INTERVAL_NSEC = 100_000_000;
    private static final int SPEED = 300;
    private final FrameAnimationBitmap fabNormal;
    private final FrameAnimationBitmap fabLeft;
    private final FrameAnimationBitmap fabRight;
    private final int damage;
    private int life;

    private float speed;
    private float base;
    private long lastFire;

    public E_Virus(float x, float y, float dx, float dy, int stageNum) {
        super(x, y, 150, 150, R.mipmap.enemy_red, 0, 7);
        base = y;
        fabNormal = fab;
        fabLeft = new FrameAnimationBitmap(R.mipmap.enemy_red, 12, 1);
        fabRight = new FrameAnimationBitmap(R.mipmap.enemy_yellow, 12, 1);
        life = 2;
        maxlife = 2;
        damage = 1;
        this.dx = dx;
        this.dy = dy;
        this.stageNum = stageNum;
        bar = new UI_Bar(x-60, y+100);
        hp = new UI_HP(x-60, y+100,life,maxlife);

        if(stageNum==1) {
            Stage_1.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), hp);
            Stage_1.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), bar);
        }

        if(stageNum==2) {
            Stage_2.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), hp);
            Stage_2.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), bar);
        }

        if(stageNum==3) {
            Stage_3.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), hp);
            Stage_3.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), bar);
        }
        if(stageNum==4) {
            Tutorial.get().getGameWorld().add(Tutorial.Layer.ui.ordinal(), hp);
            Tutorial.get().getGameWorld().add(Tutorial.Layer.ui.ordinal(), bar);
        }
    }
    public void removeUI(){
        hp.remove();
        bar.remove();}
    @Override
    public int getLife() { return life;
    }

    @Override
    public void setLife(float life) {
        this.life +=  life;
    }

    @Override
    public int getDamage() {
        return damage;
    }

    @Override
    public void setDamage(int damage) {
        this.life -= damage;
        SoundEffects.get().play(R.raw.die_sound);
        if(this.life <= 0) {
            remove();
            removeUI();

            Effect effect = new Effect(x, y, 200, 200, R.mipmap.dead6, 40, 16, 1);

            if(stageNum==1) {
                Stage_1.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), effect);
                Stage_1.get().setScoreObject(100);}
            else if(stageNum==2) {
                Stage_2.get().getGameWorld().add(Stage_2.Layer.ui.ordinal(), effect);
                Stage_2.get().setScoreObject(100);}
            else if(stageNum==3) {
                Stage_3.get().getGameWorld().add(Stage_3.Layer.ui.ordinal(), effect);
                Stage_3.get().setScoreObject(100);}
            else if(stageNum==4) {
                Tutorial.get().getGameWorld().add(Tutorial.Layer.ui.ordinal(), effect);
                Tutorial.get().setScoreObject(100);}

        }
    }

    public enum AnimState {
        normal, left, right
    }

    public void setAnimState(AnimState state) {
        if (state == AnimState.normal) {
            fab = fabNormal;
        } else if (state == AnimState.left){
            fab = fabLeft;
        } else if (state == AnimState.right){
            fab = fabRight;
        }
    }

    @Override
    public void update() {
        bar.setPos(x-50,y+100);
        hp.setPos(x-50,y+100,life);
        float seconds = GameTimer.getTimeDiffSeconds();
        x += dx * seconds * VIRUS_DROP_SPPED;
        float radius = getRadius();
        int screenWidth = UiBridge.metrics.size.x;
        int screenHeight = UiBridge.metrics.size.y;
//        Log.d(TAG, "dx=" + dx + " nanos=" + nanos + " x=" + x);
        if (dx > 0 && x > screenWidth - radius) {
            dx *= -1;
            x = screenWidth - radius;
        }
        if (dx < 0 && x < radius) {
            dx *= -1;
            x = radius;
        }
        y += dy * seconds * VIRUS_DROP_SPPED;
        if (dy > 0 && y > screenHeight + radius) {
            remove();
            //아래선 넘어가면 죽음
        }
        if (dy < 0 && y < radius) {
            dy *= -1;
            y = radius;
        }
    }

    @Override
    public void getBox(RectF rect) {
        int hw = width / 2;
        int hh = height / 2;
        rect.left = x - hw;
        rect.top = y - hh;
        rect.right = x + hw;
        rect.bottom = y + hh;
    }
}
