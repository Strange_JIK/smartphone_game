package kr.ac.kpu.game.scgyong.gameskeleton.game.scene;

import android.widget.ListView;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameScene;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.UiBridge;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.BitmapObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.ui.Button;

public class HighScoreScene extends GameScene {
    private static final String TAG = HighScoreScene.class.getSimpleName();

    public enum Layer {
        bg, enemy, player, ui, COUNT
    }

    @Override
    protected int getLayerCount() {
        return Layer.COUNT.ordinal();
    }

    @Override
    public void enter() {
        super.enter();
        setTransparent(true);
        initObjects();
    }


    private void initObjects() {
        int screenWidth = UiBridge.metrics.size.x;
        int screenHeight = UiBridge.metrics.size.y;

        int cx = UiBridge.metrics.center.x;
        int y = UiBridge.metrics.center.y-300;

        gameWorld.add(Layer.bg.ordinal(), new BitmapObject(cx, y+300, screenWidth, screenHeight, R.mipmap.black_transparent));
        Button btn_exit = new Button(cx, y, R.mipmap.btn_exit, R.mipmap.blue_round_btn, R.mipmap.red_round_btn);
        btn_exit.setOnClickRunnable(new Runnable() {
            @Override
            public void run() {
              pop();

            }
        });
        gameWorld.add(Layer.ui.ordinal(), btn_exit);
////////////////////////
    }
}
