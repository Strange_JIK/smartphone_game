package kr.ac.kpu.game.scgyong.gameskeleton.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.Date;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.input.sensor.GyroSensor;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameScene;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.UiBridge;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.res.sound.SoundEffects;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.view.GameView;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Temp.HighScoreItem;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Temp.Serializer;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Start_Menu;

public class GameActivity extends AppCompatActivity {

    private static final long BACKKEY_INTERVAL_MSEC = 1000;
    private static final String TAG =GameActivity.class.getSimpleName();
    private Start_Menu startMenu;
    private int score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        UiBridge.setActivity(this);
        super.onCreate(savedInstanceState);
        setContentView(new GameView(this));

        SoundEffects se = SoundEffects.get();
        se.getClass();
        se.loadAll(this);

            startMenu = new Start_Menu();
            startMenu.run();
            startMenu.setActivity(this);

    }

    private long lastBackPressedOn;
    @Override
    public void onBackPressed() {
        long now = System.currentTimeMillis();
        long elapsed = now - lastBackPressedOn;
        if (elapsed <= BACKKEY_INTERVAL_MSEC) {
            handleBackPressed();
            return;
        }
        Log.d("BackKey", "elapsed="+elapsed);
        Toast.makeText(this,
                "Press Back key twice quickly to exit",
                Toast.LENGTH_SHORT)
                .show();
        lastBackPressedOn = now;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (GyroSensor.isCreated()) {
            GyroSensor.get().register();
        }
    }

    @Override
    protected void onPause() {
        if (GyroSensor.isCreated()) {
            GyroSensor.get().unregister();
        }
        super.onPause();
    }

    public void handleBackPressed() {
        GameScene.getTop().onBackPressed();

    }

    public void onBtnAdd(View view) {
        startMenu.setScoreValue(this.score);
        startMenu.onBtnAdd(view);
    }

    public void onBtnDelete(View view) {
        startMenu.onBtnDelete(view);
    }

    public void addHighscore(String name, int score){
        startMenu.addHighscore(name,score);
    }

    public void onBtnExit(View view) {
        setContentView(new GameView(this));
    }

    public void setScore(int score) {
        this.score = score;
    }
}
