package kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Temp;

import android.graphics.RectF;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.iface.BoxCollidable;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameTimer;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.UiBridge;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.AnimObject;

public class Item extends AnimObject implements BoxCollidable {
    private static final float ITEM_DROP_SPEED = 3;
    protected float dx, dy;
    public Item(float x, float y, float dx, float dy) {
        super(x, y, 50, 50, R.mipmap.fireball_128_24f, 10, 1);
        this.dx = dx;
        this.dy = dy;
    }

    @Override
    public float getRadius() {
        return this.width / 4;
    }

    public void update() {
        float seconds = GameTimer.getTimeDiffSeconds();
        x += dx * seconds * ITEM_DROP_SPEED;
        float radius = getRadius();
        int screenWidth = UiBridge.metrics.size.x;
        int screenHeight = UiBridge.metrics.size.y;
//        Log.d(TAG, "dx=" + dx + " nanos=" + nanos + " x=" + x);
        if (dx > 0 && x > screenWidth - radius) {
            dx *= -1;
            x = screenWidth - radius;
        }
        if (dx < 0 && x < radius) {
            dx *= -1;
            x = radius;
        }
        y += dy * seconds * ITEM_DROP_SPEED;
        if (dy > 0 && y > screenHeight + radius) {
            //바닥을 지나면 삭제
            remove();
        }
        if (dy < 0 && y < radius) {
            dy *= -1;
            y = radius;
        }
    }

    @Override
    public void getBox(RectF rect) {
        int hw = width / 2;
        int hh = height / 2;
        rect.left = x - hw;
        rect.top = y - hh;
        rect.right = x + hw;
        rect.bottom = y + hh;
    }
}
