package kr.ac.kpu.game.scgyong.gameskeleton.game.obj;

import android.graphics.RectF;

import java.util.ArrayList;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.iface.BoxCollidable;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.GameTimer;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.main.UiBridge;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.AnimObject;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.util.CollisionHelper;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.enemy_generator.E_Guided_Virus;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.enemy_generator.E_Mitosis_Virus;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.enemy_generator.E_Stage1_Boss;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.enemy_generator.E_Stage2_Boss;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.enemy_generator.E_Stage3_Boss;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.enemy_generator.E_Virus;
import kr.ac.kpu.game.scgyong.gameskeleton.game.obj.hero_generator.H_Vaccine;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_1;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_2;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Stage_3;
import kr.ac.kpu.game.scgyong.gameskeleton.game.scene.Tutorial;

public class Bullet extends AnimObject implements BoxCollidable {
    private final int damage;
    private final boolean hero_bullet;
    private final int stageNum;
    protected float dx, dy;
    //x 좌표, y 좌표, 리소스 id, x방향 속도, y방향 속도, 데미지, 내총알/적총알
    public Bullet(float x, float y, int width, int height, int bullet_id, float dx, float dy, int damage, boolean hero_bullet, int stageNum, int count) {
        super(x, y, width, height, bullet_id, 10, count);
        this.dx = dx;
        this.dy = dy;
        this.damage = damage;
        this.hero_bullet = hero_bullet;
        this.stageNum = stageNum;
    }
    public Bullet(float x, float y, int width, int height, int bullet_id,float dx, float dy,int damage, boolean hero_bullet, int stageNum) {
        super(x, y, width, height, bullet_id, 10, 1);
        this.dx = dx;
        this.dy = dy;
        this.damage = damage;
        this.hero_bullet = hero_bullet;
        this.stageNum = stageNum;
    }
    public boolean getBulletType() {
        return hero_bullet;
    }

    @Override
    public float getRadius() {
        return this.width / 4;
    }

    public void update() {
        float seconds = GameTimer.getTimeDiffSeconds();
        //x += dx * seconds;
        float radius = getRadius();
        int screenWidth = UiBridge.metrics.size.x;
        int screenHeight = UiBridge.metrics.size.y;
//        Log.d(TAG, "dx=" + dx + " nanos=" + nanos + " x=" + x);
        x += dx * seconds;
        if (dx > 0 && x > screenWidth - radius) {
            SelfRemove();
        }
        if (dx < 0 && x < radius) {
            SelfRemove();
        }
        y += dy * seconds;
        if (dy > 0 && y > screenHeight - radius) {
            SelfRemove();
        }
        if (dy < 0 && y < radius) {
            SelfRemove();
        }
        if(hero_bullet){ checkEnemyCollision();}
        else {checkHeroCollision();}
    }
    //적총알 맞는 체크
    private void checkHeroCollision(){
        if(stageNum == 1) {
            ArrayList<GameObject> Enemies = Stage_1.get().getGameWorld().objectsAtLayer(Stage_1.Layer.player.ordinal());
            for (GameObject obj : Enemies) {
                H_Vaccine vaccne = (H_Vaccine) obj;
                if (CollisionHelper.collides(this, vaccne)) {
                    vaccne.setDamage(damage);
                    SelfRemove();
                }
            }
        }
        else if(stageNum == 2) {
            ArrayList<GameObject> Enemies = Stage_2.get().getGameWorld().objectsAtLayer(Stage_2.Layer.player.ordinal());
            for (GameObject obj : Enemies) {
                H_Vaccine vaccne = (H_Vaccine) obj;
                if (CollisionHelper.collides(this, vaccne)) {
                    vaccne.setDamage(damage);
                    SelfRemove();
                }
            }
        }
        else if(stageNum == 3) {
            ArrayList<GameObject> Enemies = Stage_3.get().getGameWorld().objectsAtLayer(Stage_3.Layer.player.ordinal());
            for (GameObject obj : Enemies) {
                H_Vaccine vaccne = (H_Vaccine) obj;
                if (CollisionHelper.collides(this, vaccne)) {
                    vaccne.setDamage(damage);
                    SelfRemove();
                }
            }
        }
        else if(stageNum == 4) {
            ArrayList<GameObject> Enemies = Tutorial.get().getGameWorld().objectsAtLayer(Tutorial.Layer.player.ordinal());
            for (GameObject obj : Enemies) {
                H_Vaccine vaccne = (H_Vaccine) obj;
                if (CollisionHelper.collides(this, vaccne)) {
                    vaccne.setDamage(damage);
                    SelfRemove();
                }
            }
        }
    }
    private void checkEnemyCollision() {
        if(stageNum == 1) {
            ArrayList<GameObject> Enemies = Stage_1.get().getGameWorld().objectsAtLayer(Stage_1.Layer.enemy.ordinal());
            for (GameObject obj : Enemies) {
                if (!(obj instanceof E_Stage1_Boss)) {
                    continue;
                }
                E_Stage1_Boss Boss = (E_Stage1_Boss) obj;
                if (CollisionHelper.collides(this, Boss)) {
                    Boss.setDamage(damage);
                    SelfRemove();
                }
            }
            for (GameObject obj : Enemies) {
                if (!(obj instanceof E_Virus)) {
                    continue;
                }
                E_Virus EVirus = (E_Virus) obj;
                if (CollisionHelper.collides(this, EVirus)) {
                    EVirus.setDamage(damage);
                    SelfRemove();
                }
            }

            for (GameObject obj : Enemies) {
                if (!(obj instanceof E_Mitosis_Virus)) {
                    continue;
                }
                E_Mitosis_Virus EVirus = (E_Mitosis_Virus) obj;
                if (CollisionHelper.collides(this, EVirus)) {
                    EVirus.setDamage(damage);
                    SelfRemove();
                }
            }
            for (GameObject obj : Enemies) {
                if (!(obj instanceof E_Guided_Virus)) {
                    continue;
                }
                E_Guided_Virus EVirus = (E_Guided_Virus) obj;
                if (CollisionHelper.collides(this, EVirus)) {
                    EVirus.setDamage(damage);
                    SelfRemove();
                }
            }
        }
        else if(stageNum == 2) {
            ArrayList<GameObject> Enemies = Stage_2.get().getGameWorld().objectsAtLayer(Stage_2.Layer.enemy.ordinal());
            for (GameObject obj : Enemies) {
                if (!(obj instanceof E_Stage2_Boss)) {
                    continue;
                }
                E_Stage2_Boss Boss = (E_Stage2_Boss) obj;
                if (CollisionHelper.collides(this, Boss)) {
                    Boss.setDamage(damage);
                    SelfRemove();
                }
            }
            for (GameObject obj : Enemies) {
                if (!(obj instanceof E_Virus)) {
                    continue;
                }
                E_Virus EVirus = (E_Virus) obj;
                if (CollisionHelper.collides(this, EVirus)) {
                    EVirus.setDamage(damage);
                    SelfRemove();
                }
            }
            for (GameObject obj : Enemies) {
                if (!(obj instanceof E_Mitosis_Virus)) {
                    continue;
                }
                E_Mitosis_Virus EVirus = (E_Mitosis_Virus) obj;
                if (CollisionHelper.collides(this, EVirus)) {
                    EVirus.setDamage(damage);
                    SelfRemove();
                }
            }
            for (GameObject obj : Enemies) {
                if (!(obj instanceof E_Guided_Virus)) {
                    continue;
                }
                E_Guided_Virus EVirus = (E_Guided_Virus) obj;
                if (CollisionHelper.collides(this, EVirus)) {
                    EVirus.setDamage(damage);
                    SelfRemove();
                }
            }
        }
        else if(stageNum == 3) {
            ArrayList<GameObject> Enemies = Stage_3.get().getGameWorld().objectsAtLayer(Stage_3.Layer.enemy.ordinal());
            for (GameObject obj : Enemies) {
                if (!(obj instanceof E_Stage3_Boss)) {
                    continue;
                }
                E_Stage3_Boss Boss = (E_Stage3_Boss) obj;
                if (CollisionHelper.collides(this, Boss)) {
                    Boss.setDamage(damage);
                    SelfRemove();
                }
            }
            for (GameObject obj : Enemies) {
                if (!(obj instanceof E_Virus)) {
                    continue;
                }
                E_Virus EVirus = (E_Virus) obj;
                if (CollisionHelper.collides(this, EVirus)) {
                    EVirus.setDamage(damage);
                    SelfRemove();
                }
            }
            for (GameObject obj : Enemies) {
                if (!(obj instanceof E_Mitosis_Virus)) {
                    continue;
                }
                E_Mitosis_Virus EVirus = (E_Mitosis_Virus) obj;
                if (CollisionHelper.collides(this, EVirus)) {
                    EVirus.setDamage(damage);
                    SelfRemove();
                }
            }

            for (GameObject obj : Enemies) {
                if (!(obj instanceof E_Guided_Virus)) {
                    continue;
                }
                E_Guided_Virus EVirus = (E_Guided_Virus) obj;
                if (CollisionHelper.collides(this, EVirus)) {
                    EVirus.setDamage(damage);
                    SelfRemove();
                }
            }
        }
        else if(stageNum == 4) {
            ArrayList<GameObject> Enemies = Tutorial.get().getGameWorld().objectsAtLayer(Tutorial.Layer.enemy.ordinal());
            for (GameObject obj : Enemies) {
                if (!(obj instanceof E_Virus)) {
                    continue;
                }
                E_Virus EVirus = (E_Virus) obj;
                if (CollisionHelper.collides(this, EVirus)) {
                    EVirus.setDamage(damage);
                    SelfRemove();
                }
            }
        }
    }

    void SelfRemove(){
        if(hero_bullet==true) {
            Effect effect = new Effect(x, y, 200, 200, R.mipmap.pop, 40, 6, 1);
            if(stageNum==1) {
                Stage_1.get().getGameWorld().add(Stage_1.Layer.ui.ordinal(), effect); }
            else if(stageNum==2) {
                Stage_2.get().getGameWorld().add(Stage_2.Layer.ui.ordinal(), effect); }
            else if(stageNum==3) {
                Stage_3.get().getGameWorld().add(Stage_3.Layer.ui.ordinal(), effect); }
            else if(stageNum==4) {
                Tutorial.get().getGameWorld().add(Tutorial.Layer.ui.ordinal(), effect); }
        }
        this.remove();
    }

    @Override
    public void getBox(RectF rect) {
        int hw = width / 2;
        int hh = height / 2;
        rect.left = x - hw;
        rect.top = y - hh;
        rect.right = x + hw;
        rect.bottom = y + hh;
    }
}
