package kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Temp;

import android.util.Log;
import android.view.MotionEvent;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.AnimObject;

public class T_Joystick extends AnimObject {
    private static final String TAG = T_Joystick.class.getSimpleName();
    private float x;
    private final float y;
    private boolean down;
    private float xDown,yDown;
    private double angle;

    public T_Joystick(float x, float y) {
        super(x, y, -50, -50, R.mipmap.joystick, 10, 1);
        this.x = x;
        this.y = y;
        this.down = false;
    }

    @Override
    public void update() {
    }

    public void onTouchEvent(MotionEvent event)
    {
        switch (event.getAction())
        {
        case MotionEvent.ACTION_DOWN:
            xDown = event.getX();
            yDown = event.getY();
            //if()
            down = true;
            break;

        case MotionEvent.ACTION_MOVE:
            //if(!down)
              //  return;
            float dx = event.getX()- xDown;
            float dy = event.getY()- yDown;
            this.angle = Math.atan2(dy,dx);//*(180/Math.PI);
            Log.d(TAG,"angle : "+angle);
            break;
        default:
            down = false;
        }
    }

    public int getHorzDirection() {
        if (!down) return 0;
        int dir = angle < Math.PI / 2 && angle > -Math.PI / 2 ? 1 : -1;
        Log.v(TAG, "Dir = " + dir);
        return dir;
    }

}
