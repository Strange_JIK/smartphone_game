package kr.ac.kpu.game.scgyong.gameskeleton.game.obj.stage_obj;

import kr.ac.kpu.game.scgyong.gameskeleton.R;
import kr.ac.kpu.game.scgyong.gameskeleton.framework.obj.bg.ImageScrollBackground;

public class CellBackGround extends ImageScrollBackground {
    public CellBackGround() {
        super(R.mipmap.cell_map, Orientation.vertical, 300);
    }
}
