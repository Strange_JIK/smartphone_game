package kr.ac.kpu.game.scgyong.gameskeleton.game.obj.Temp;

import android.util.Log;

import kr.ac.kpu.game.scgyong.gameskeleton.framework.input.sensor.GyroSensor;

public class T_GyroTBall extends T_Ball {
    private static final int DEGREE_TO_SPEED_MULTIPLIER = 50;
    public T_GyroTBall(float x, float y) {
        super(x, y, 0, 0);
    }

    @Override
    public void update() {
        GyroSensor gyro = GyroSensor.get();
        this.dx = gyro.getPitchDegree() * DEGREE_TO_SPEED_MULTIPLIER;
        this.dy = gyro.getRollDegree() * DEGREE_TO_SPEED_MULTIPLIER;
        Log.d("GyroBall", dx + ", " + dy);
        super.update();
    }
}
